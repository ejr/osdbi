VERSION=$(shell awk '/Version:/ { print $$2; }' DESCRIPTION)

all:	src/osdbi.oct PKG_ADD docs
.PHONY:	all

src/osdbi.oct:	src/osdb*.cc src/osdb*.h
	make -C src osdbi.oct

PKG_ADD:	src/osdb*.cc
	perl -ne 'if (/PKG_ADD:\s+(.+)$$/) { $$ln = $$1; $$ln =~ s,osdbi.oct,src/osdbi.oct,g; print $$ln, "\n";}' $^ > $@

pkg:	osdbi-$(VERSION).tar.gz
.PHONY:	pkg

osdbi-$(VERSION).tar.gz:
	git archive --format=tar --prefix=osdbi-$(VERSION)/ HEAD | gzip -9 > $@

docs:
	make -C doc
.PHONY:	docs

clean:
	make -C src clean
	make -C doc clean
.PHONY:	clean

