1;
#osdbi_toggle_tracing ();
zz = osdbi ("sqlite3")
osdbi_set_option (zz, "filename", "tst.sqlite3")
[err, msg] = unlink ("tst.sqlite3");
osdbi_open (zz)
disp (zz)

osdbi_do (zz, "create table stringtst (a real, b integer, c text);");
indata = struct ("a", 0.0, "b", 1, "c", "hello");
osdbi_insert (zz, "stringtst", indata)

osdbi_do (zz, "create table stringtst2 (a text, b integer, c text);");
indata = struct ("a", "quux", "b", 1, "c", "hello");
osdbi_insert (zz, "stringtst2", indata)

osdbi_do (zz, "create table aa (a, b);")
osdbi_do (zz, "insert into aa (a, b) values (22, 23);")
disp (zz)
indata = struct ("a", int32(99), "b", int32(99))
osdbi_insert (zz, "aa", indata)
indata = struct ("a", int32([1,2,3,4]), "b", 5:8)
osdbi_insert (zz, "aa", indata)
oh = {"foo", "quux"};
hell = {"bar", "baz"};
#indata = struct ("a", {{"foo", "quux"}}, "b", {{"bar", "baz"}})
indata = struct ("a", {oh}, "b", {hell})
osdbi_insert (zz, "aa", indata, "a")

osdbi_do (zz, "create table arytst (a, b, c);")
indata = struct ("a", {{1:3, [2, .7]}}, "b", {{ones(5), randn(2)}}, "c", 77);
osdbi_insert (zz, "arytst", indata);

osdbi_expand (zz, "arytst")

q = struct ("a", 99)
qq = osdbi_expand (zz, "aa", q)
qq2 = osdbi_expand (zz, "aa", q, "b")
qq3 = osdbi_expand (zz, "aa")

q2 = struct ("a", {{-100, "foo"}})
osdbi_expand (zz, "aa", q2)

osdbi_do (zz, "select count(*) from aa;")

osdbi_do (zz, "select * from aa limit 3;")

osdbi_do (zz, "select * from aa limit 1;")

[a, b] = osdbi_do (zz, "select * from aa limit 2;")

osdbi_do (zz, "create table whee(id integer primary key, a);");
indata = struct ("a", 1:8);
osdbi_insert (zz, "whee", indata, "id")
indata = struct ("a", 99);
osdbi_insert (zz, "whee", indata, "id")

osdbi_expand (zz, "whee", struct("a", [2 3 4]), "id", ":not", struct("id", 3))

osdbi_close (zz)
disp (zz)
