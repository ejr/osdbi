/* Copyright (c) 2008, Jason Riedy <jason@acm.org>.  See COPYING file for license. */
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include <memory.h>

#include <alloca.h>

#include <stdio.h>

/* Note: bzip2 adds more overhead to small arrays and is much slower... 
   lzo1x (in minilzo) is fast but often fails to compress substantially.
   lzma is not wide-spread enough.
*/
#if !defined(USE_BZ2)
#define USE_ZLIB 1
#endif

#if defined(USE_ZLIB)
#include <zlib.h>
#elif defined(USE_BZ2)
#include <bzlib.h>
#else
#error "Define *some* compressor"
#endif

#include "mtxcodec.h"

#if !defined(__cplusplus__)
#define FORCE_CAST(t,v) ((t)(v))
#else
#define FORCE_CAST(t,v) reinterpret_cast<t>(v)
#endif

#if !defined(FLOAT_COMPLEX)
#define FLOAT_COMPLEX float _Complex
#endif
#if !defined(DOUBLE_COMPLEX)
#define DOUBLE_COMPLEX double _Complex
#endif

/*
  Note that compression only prevents losing too much space.  To
  possibly *gain* space, the stuffer would need to store exponents and
  significands separately (at the very least).

  basic format:
    version 0:
      | version | comp. len | uncomp. len | tag    | ndim  | dim[0]...dim[ndim-1] | data                                  |
      | 8-bit   | 64-bit    | 64-bit      | [8-bit | 8-bit | ndim 64-bit          | 16-bit signed,biased exp; 64-bit sig] |
*/

/* Only support writing the current version */
#define OUTPUT_VERSION 0

static size_t
stuff_int16 (uint8_t* buf, int16_t datum_in)
{
    uint16_t datum = (uint16_t)datum_in;
    for (int k = 0; k < sizeof(uint16_t); ++k) {
	buf[k] = (uint8_t)(datum & 0xFFu);
	datum >>= 8;
    }
    return sizeof(uint16_t);
}

static size_t
unstuff_int16 (const uint8_t* buf, int16_t* datum_out)
{
    uint16_t datum = 0;
    for (int k = 0; k < sizeof(uint16_t); ++k) {
	uint16_t to_or = buf[k];
	to_or <<= 8*k;
	datum |= to_or;
    }
    *datum_out = (int16_t)datum;
    return sizeof(uint16_t);
}

static size_t
stuff_int32 (uint8_t* buf, int32_t datum_in)
{
    uint32_t datum = (uint32_t)datum_in;
    for (int k = 0; k < sizeof(uint32_t); ++k) {
	buf[k] = (uint8_t)(datum & 0xFFu);
	datum >>= 8;
    }
    return sizeof(uint32_t);
}

static size_t
unstuff_int32 (const uint8_t* buf, int32_t* datum_out)
{
    uint32_t datum = 0;
    for (int k = 0; k < sizeof(uint32_t); ++k) {
	uint32_t to_or = buf[k];
	to_or <<= 8*k;
	datum |= to_or;
    }
    *datum_out = (int32_t)datum;
    return sizeof(uint32_t);
}

static size_t
stuff_uint64 (uint8_t* buf, uint64_t datum)
{
    for (int k = 0; k < sizeof(uint64_t); ++k) {
	buf[k] = (uint8_t)(datum & 0xFFu);
	datum >>= 8;
    }
    return sizeof(uint64_t);
}

static size_t
unstuff_uint64 (const uint8_t* buf, uint64_t* datum_out)
{
    uint64_t datum = 0;
    for (int k = 0; k < sizeof(uint64_t); ++k) {
	uint64_t to_or = buf[k];
	to_or <<= 8*k;
	datum |= to_or;
    }
    *datum_out = datum;
    return sizeof(uint64_t);
}

static size_t
stuff_double (uint8_t* buf, const size_t ndbl, double val)
{
    int sz = 0;
    int exp;
    uint64_t sig;
    double aval = fabs(val), tmp, sgn = 1.0;
    tmp = frexp (aval, &exp);
    exp += 1024;
    sig = lrint (ldexp (tmp, 53)); /* Now a non-negative integer */
    sgn = copysign (sgn, val);
    if (sgn < 0) exp = -exp;
    sz = stuff_int16 (buf, (int16_t)exp);
    sz += stuff_uint64 (buf+sz, (uint64_t)sig);

#if 0
    {
	double tst = 0;
	if (exp < 0) {
	    sgn = -1.0;
	    exp = -exp - 1024;
	}
	else {
	    sgn = 1.0;
	    exp -= 1024;
	}
	tst = sgn * ldexp (sig, exp-53);
	if (tst != val) {
	    fprintf (stderr, "HORKED\n");
	}
    }
#endif

    return sz;
}

static size_t
unstuff_double (const uint8_t* buf, const size_t ndbl, double* val)
{
    int sz = 0;
    int16_t stored_exp;
    int exp;
    uint64_t sig;
    double sgn, out;

    sz = unstuff_int16 (buf, &stored_exp);
    sz += unstuff_uint64 (buf+sz, &sig);

    if (stored_exp < 0) {
	sgn = -1.0;
	exp = -stored_exp - 1024;
    }
    else {
	sgn = 1.0;
	exp = stored_exp - 1024;
    }

    out = sgn * ldexp (sig, exp-53);
    *val = out;
    return sz;
}

static size_t
dim_nelem (const int ndim, const size_t* dim)
{
    size_t nelem = 1;
    for (int k = 0; k < ndim; ++k)
	nelem *= dim[k];
    return nelem;
}

MTXCDC_EMBED size_t
mtxcdc_dim_nelem (const int ndim, const size_t* dim)
{
    return dim_nelem (ndim, dim);
}

static size_t
worst_uncompressed_len (const uint8_t ndim, const size_t* dim, const size_t elemsz)
{
    size_t out = 2; /* 8-bit tag, 8-bit ndim */
    size_t nelem = dim_nelem (ndim, dim);
    size_t printsz = sizeof(uint64_t) + sizeof(int16_t);
    //size_t printsz = 1+4+1+1+14+3+1;
    //size_t printsz = 30;
    out += ndim*sizeof(uint64_t) + elemsz * nelem * printsz;
    return out;
}

static int
is_complex (const int tag)
{
    if (tag == MTXCDC_COMPLEX_DOUBLE || tag == MTXCDC_COMPLEX_FLOAT) return 1;
    return 0;
}

MTXCDC_EMBED size_t
mtxenc_worst_uncompressed_len (const int tag, const int ndim, const size_t* dim)
{
    const size_t elemsz = (is_complex (tag)? 2 : 1);
    if (ndim > 255) return 0;
    size_t out = worst_uncompressed_len ((uint8_t)ndim, dim, elemsz);
    return out;
}

static uint8_t*
encode_uncompressed_header (uint8_t* buf, const uint8_t tag, const uint8_t ndim, const size_t* dim)
{
    *buf++ = tag;
    *buf++ = ndim;
    for (int k = 0; k < ndim; ++k)
	buf += stuff_uint64 (buf, dim[k]);
    return buf;
}

static uint8_t*
encode_uncompressed_ui8 (uint8_t* buf, const uint8_t ndim, const size_t* dim, const uint8_t *data)
{
    size_t nelem = dim_nelem (ndim, dim);
    buf = encode_uncompressed_header (buf, MTXCDC_UINT8, ndim, dim);
    memcpy (buf, data, nelem);
    return buf + nelem;
}

static uint8_t*
encode_uncompressed_i (uint8_t* buf, const uint8_t ndim, const size_t* dim, const int *data)
{
    size_t nelem = dim_nelem (ndim, dim);
    buf = encode_uncompressed_header (buf, MTXCDC_INT32, ndim, dim);
    for (size_t k = 0; k < nelem; ++k)
	buf += stuff_int32 (buf, data[k]);
    return buf;
}

static uint8_t*
encode_uncompressed_d (uint8_t* buf, const uint8_t ndim, const size_t* dim, const double *data)
{
    size_t nelem = dim_nelem (ndim, dim);
    buf = encode_uncompressed_header (buf, MTXCDC_REAL_DOUBLE, ndim, dim);
    for (size_t k = 0; k < nelem; ++k)
	buf += stuff_double (buf, nelem, data[k]);
    return buf;
}

static uint8_t*
encode_uncompressed_s (uint8_t* buf, const uint8_t ndim, const size_t* dim, const float *data)
{
    size_t nelem = dim_nelem (ndim, dim);
    buf = encode_uncompressed_header (buf, MTXCDC_REAL_FLOAT, ndim, dim);
    for (size_t k = 0; k < nelem; ++k)
	buf += stuff_double (buf, nelem, data[k]);
    return buf;
}

static uint8_t*
encode_uncompressed_z (uint8_t* buf, const uint8_t ndim, const size_t* dim, const DOUBLE_COMPLEX* data)
{
    const double *data_alias = FORCE_CAST(const double*, data);
    size_t nelem = 2*dim_nelem (ndim, dim);
    buf = encode_uncompressed_header (buf, MTXCDC_COMPLEX_DOUBLE, ndim, dim);
    for (size_t k = 0; k < nelem; ++k)
	buf += stuff_double (buf, nelem, data_alias[k]);
    return buf;
}

static uint8_t*
encode_uncompressed_c (uint8_t* buf, const uint8_t ndim, const size_t* dim, const FLOAT_COMPLEX* data)
{
    const float *data_alias = FORCE_CAST(const float*, data);
    size_t nelem = 2*dim_nelem (ndim, dim);
    buf = encode_uncompressed_header (buf, MTXCDC_COMPLEX_FLOAT, ndim, dim);
    for (size_t k = 0; k < nelem; ++k)
	buf += stuff_double (buf, nelem, data_alias[k]);
    return buf;
}

MTXCDC_EMBED uint8_t*
mtxenc_pack (uint8_t* buf, size_t* blen, const int tag, const int ndim, const size_t* dim, const void* data)
{
    uint8_t* buf_out = NULL;
    switch (tag) {
    case MTXCDC_UINT8:
	buf_out = encode_uncompressed_ui8 (buf, ndim, dim, FORCE_CAST(uint8_t*, data));
	break;
    case MTXCDC_INT32:
	buf_out = encode_uncompressed_i (buf, ndim, dim, FORCE_CAST(int*, data));
	break;
    case MTXCDC_REAL_DOUBLE:
	buf_out = encode_uncompressed_d (buf, ndim, dim, FORCE_CAST(double*, data));
	break;
    case MTXCDC_REAL_FLOAT:
	buf_out = encode_uncompressed_s (buf, ndim, dim, FORCE_CAST(float*, data));
	break;
    case MTXCDC_COMPLEX_DOUBLE:
	buf_out = encode_uncompressed_z (buf, ndim, dim, FORCE_CAST(DOUBLE_COMPLEX*, data));
	break;
    case MTXCDC_COMPLEX_FLOAT:
	buf_out = encode_uncompressed_c (buf, ndim, dim, FORCE_CAST(FLOAT_COMPLEX*, data));
	break;
    }
    if (buf_out) *blen = (size_t)(buf_out - buf);
    return buf_out;
}

static size_t
worst_framed_len (const uint8_t* buf, const size_t blen)
{
#if defined(USE_ZLIB)
    return 1+2*sizeof(uint64_t) + compressBound (blen);
#elif defined(USE_BZ2)
    return 1+2*sizeof(uint64_t) + blen + (blen+99)/100 + 600;
#endif
}

MTXCDC_EMBED size_t
mtxenc_worst_framed_len (const uint8_t* buf, const size_t blen)
{
    return worst_framed_len (buf, blen);
}

static uint8_t*
frame_block (uint8_t* framed, size_t* framedlen_out, const uint8_t* buf, const size_t blen)
{
    uint8_t* complen_place;
    size_t framedlen, sz;

    *framed++ = 0; /* version 0 */
    framedlen = 1;
    complen_place = framed;
    sz = stuff_uint64 (framed, 0); /* complen placeholder */
    framed += sz;
    framedlen += sz;
    sz = stuff_uint64 (framed, blen); /* uncomplen */
    framed += sz;
    framedlen += sz;

#if defined(USE_ZLIB)
    uLongf outlen = worst_framed_len (buf, blen);
    int err = compress (framed, &outlen, buf, blen);
    if (err != Z_OK) return NULL;
#elif defined(USE_BZ2)
    unsigned int outlen = worst_framed_len (buf, blen);
    int err = BZ2_bzBuffToBuffCompress (FORCE_CAST(char*, framed), &outlen, FORCE_CAST(char*, buf), blen, 9, 0, 0);
    if (err != BZ_OK) return NULL;
#endif
    framedlen += outlen;
    framed += outlen;
    stuff_uint64 (complen_place, outlen);

    *framedlen_out = framedlen;
    return framed;
}

MTXCDC_EMBED uint8_t*
mtxenc_frame (uint8_t* framed, size_t* framedlen, const uint8_t* buf, const size_t blen)
{
    return frame_block (framed, framedlen, buf, blen);
}

struct framehead {
    int version;
    size_t clen, unclen;
};

static const uint8_t*
unpack_frame_header (struct framehead* f, const uint8_t* buf)
{
    f->version = *buf++;
    if (f->version != 0) return NULL;
    uint64_t tmp;
    size_t sz = unstuff_uint64 (buf, &tmp);
    f->clen = tmp;
    buf += sz;
    sz = unstuff_uint64 (buf, &tmp);
    f->unclen = tmp;
    buf += sz;
    return buf;
}

static size_t
get_uncompressed_len (const uint8_t* buf)
{
    struct framehead f;
    unpack_frame_header (&f, buf);
    return f.unclen;
}

MTXCDC_EMBED size_t
mtxdec_unframed_len (const uint8_t* framed)
{
    return get_uncompressed_len (framed);
}

static uint8_t*
copy_uncompress (uint8_t* out, const uint8_t* inbuf)
{
    struct framehead f;
    inbuf = unpack_frame_header (&f, inbuf);

#if defined(USE_ZLIB)
    uLongf outlen = f.unclen;
    int err = uncompress (out, &outlen, inbuf, f.clen);
    if (err != Z_OK) return NULL;
#elif defined(USE_BZ2)
    unsigned int outlen = f.unclen;
    int err = BZ2_bzBuffToBuffDecompress (FORCE_CAST(char*, out), &outlen, FORCE_CAST(char*, inbuf), f.clen, 0, 0);
    if (err != BZ_OK) return NULL;
#endif
    if (outlen != f.unclen) return NULL;
    return out;
}

MTXCDC_EMBED uint8_t*
mtxdec_unframe (uint8_t* unframed, const uint8_t* framed)
{
    return copy_uncompress (unframed, framed);
}

static uint8_t
unc_tag (const uint8_t* buf)
{
    return buf[0];
}

static uint8_t
unc_ndim (const uint8_t* buf)
{
    return buf[1];
}

static const uint8_t*
unc_dims (const uint8_t* buf, size_t* dim)
{
    int ndim = unc_ndim (buf);
    buf += 2;
    for (int k = 0; k < ndim; ++k) {
	uint64_t tmp;
	size_t sz = unstuff_uint64 (buf, &tmp);
	dim[k] = tmp;
	buf += sz;
    }
    return buf;
}

static size_t
decode_header (int* tag, int* ndim, size_t** dim, const uint8_t* buf)
{
    *tag = unc_tag (buf);
    *ndim = unc_ndim (buf);
    *dim = FORCE_CAST(size_t*, malloc (*ndim * sizeof (size_t)));
    unc_dims (buf, *dim);
    return 1 + 1 + *ndim * sizeof(uint64_t);
}

MTXCDC_EMBED void
mtxdec_decode_header (int* tag, int* ndim, size_t** dim, const uint8_t* buf)
{
    decode_header (tag, ndim, dim, buf);
}

static uint8_t*
unc_ui8 (uint8_t* d, const uint8_t* buf, const size_t nelem)
{
    memcpy (d, buf, nelem);
    return &d[nelem];
}

static int*
unc_i (int* d, const uint8_t* buf, const size_t nelem)
{
    for (size_t k = 0; k < nelem; ++k) {
	int32_t tmp;
	size_t sz = unstuff_int32 (buf, &tmp);
	d[k] = tmp;
	if (!sz) return NULL;
	buf += sz;
    }
    return &d[nelem];
}

static double*
unc_d (double* d, const uint8_t* buf, const size_t nelem)
{
    for (size_t k = 0; k < nelem; ++k) {
	size_t sz = unstuff_double (buf, nelem, &d[k]);
	if (!sz) return NULL;
	buf += sz;
    }
    return &d[nelem];
}

static float*
unc_s (float* d, const uint8_t* buf, const size_t nelem)
{
    for (size_t k = 0; k < nelem; ++k) {
	double tmp;
	size_t sz = unstuff_double (buf, nelem, &tmp);
	d[k] = tmp;
	if (!sz) return NULL;
	buf += sz;
    }
    return &d[nelem];
}

static DOUBLE_COMPLEX*
unc_z (DOUBLE_COMPLEX* d, const uint8_t* buf, const size_t nelem)
{
    return FORCE_CAST(DOUBLE_COMPLEX*, unc_d (FORCE_CAST(double*, d), buf, 2*nelem));
}

static FLOAT_COMPLEX*
unc_c (FLOAT_COMPLEX* d, const uint8_t* buf, const size_t nelem)
{
    return FORCE_CAST(FLOAT_COMPLEX*, unc_s (FORCE_CAST(float*, d), buf, 2*nelem));
}

MTXCDC_EMBED void*
mtxdec_decode_data (void* data, const uint8_t* buf)
{
    int tag, ndim;
    size_t *dim = NULL;
    size_t nelem;
    buf += decode_header (&tag, &ndim, &dim, buf);
    nelem = dim_nelem (ndim, dim);
    switch (tag) {
    case MTXCDC_UINT8:
	data = FORCE_CAST(void*, unc_ui8 (FORCE_CAST(uint8_t*, data), buf, nelem));
	break;
    case MTXCDC_INT32:
	data = FORCE_CAST(void*, unc_i (FORCE_CAST(int*, data), buf, nelem));
	break;
    case MTXCDC_REAL_DOUBLE:
	data = FORCE_CAST(void*, unc_d (FORCE_CAST(double*, data), buf, nelem));
	break;
    case MTXCDC_REAL_FLOAT:
	data = FORCE_CAST(void*, unc_s (FORCE_CAST(float*, data), buf, nelem));
	break;
    case MTXCDC_COMPLEX_DOUBLE:
	data = FORCE_CAST(void*, unc_z (FORCE_CAST(DOUBLE_COMPLEX*, data), buf, nelem));
	break;
    case MTXCDC_COMPLEX_FLOAT:
	data = FORCE_CAST(void*, unc_c (FORCE_CAST(FLOAT_COMPLEX*, data), buf, nelem));
	break;
    default:
	data = NULL;
	break;
    }
    free (dim);
    return data;
}

