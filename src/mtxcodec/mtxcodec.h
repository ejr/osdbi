/* Copyright (c) 2008, Jason Riedy <jason@acm.org>.  See COPYING file for license. */
#ifndef _MTXCODEC_H_
#define _MTXCODEC_H_ 1

/* If you want to bundle all this statically, define
   MTXCDC_EMBED to static. */
#if !defined(MTXCDC_EMBED)
#define MTXCDC_EMBED_EMPTY
#define MTXCDC_EMBED
#if defined(__cplusplus__)
extern "C" {
#endif
#endif

#define MTXCDC_INT32 ((uint8_t)1)
#define MTXCDC_REAL_DOUBLE ((uint8_t)2)
#define MTXCDC_COMPLEX_DOUBLE ((uint8_t)3)
#define MTXCDC_REAL_FLOAT ((uint8_t)4)
#define MTXCDC_COMPLEX_FLOAT ((uint8_t)5)
#define MTXCDC_UINT8 ((uint8_t)6)

MTXCDC_EMBED
size_t mtxcdc_dim_nelem (const int ndim, const size_t* dim);

MTXCDC_EMBED
size_t mtxenc_worst_uncompressed_len (const int /*tag*/, const int /*ndim*/, const size_t* /*dim*/);
MTXCDC_EMBED
uint8_t* mtxenc_pack (uint8_t* /*buf*/, size_t* /*blen*/, const int /*tag*/, const int /*ndim*/, const size_t* /*dim*/, const void* /*data*/);
MTXCDC_EMBED
size_t mtxenc_worst_framed_len (const uint8_t* /*buf*/, const size_t /*blen*/);
MTXCDC_EMBED
uint8_t* mtxenc_frame (uint8_t* /*framed*/, size_t* /*framedlen*/, const uint8_t* /*buf*/, const size_t /*blen*/);
MTXCDC_EMBED
size_t mtxdec_unframed_len (const uint8_t* /*framed*/);
MTXCDC_EMBED
uint8_t* mtxdec_unframe (uint8_t* /*unframed*/, const uint8_t* /*framed*/);
MTXCDC_EMBED
void mtxdec_decode_header (int* /*tag*/, int* /*ndim*/, size_t** /*dim*/, const uint8_t* /*buf*/);
MTXCDC_EMBED
void* mtxdec_decode_data (void* /*data*/, const uint8_t* /*buf*/);

#if defined(__cplusplus__) && defined(MTXCDC_EMBED_EMPTY)
} /* extern "C" */
#endif

#endif /* _MTXCODEC_H_ */
