#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <complex.h>

#include <assert.h>

#include "mtxcodec.h"

#define NCONST 10
int M = NCONST, N = NCONST;
size_t dim[2] = { NCONST, NCONST };

static void
test_double (void)
{
    double *d, *dec_d;
    uint8_t *unframed, *framed, *dec_unframed;

    size_t worst_unframed_len, worst_framed_len;
    size_t unframed_len, framed_len, dec_unframed_len;
    d = malloc (M*N*sizeof(double));
    assert (d);
    for (int k = 0; k < M*N; ++k) d[k] = drand48 ();

    dec_d = malloc (M*N*sizeof(double));
    assert (dec_d);
    for (int k = 0; k < M*N; ++k) dec_d[k] = -99.0;

    worst_unframed_len = mtxenc_worst_uncompressed_len (MTXCDC_REAL_DOUBLE, 2, dim);
    assert (worst_unframed_len);
    unframed = malloc (worst_unframed_len);
    assert (unframed);
    assert (mtxenc_pack (unframed, &unframed_len, MTXCDC_REAL_DOUBLE, 2, dim, (void*)d));
    assert (unframed_len <= worst_unframed_len);

    printf ("worst unframed: %10zi\n"
	    "      unframed: %10zi      ratio: %lg\n"
	    "double array  : %10zi      ratio: %lg\n",
	    worst_unframed_len, unframed_len, ((double)unframed_len)/worst_unframed_len,
	    M*N*sizeof (double), ((double)unframed_len)/(M*N*sizeof (double)));

    worst_framed_len = mtxenc_worst_framed_len (unframed, unframed_len);
    assert (worst_framed_len);
    framed = malloc (worst_framed_len);
    assert (framed);
    assert (mtxenc_frame (framed, &framed_len, unframed, unframed_len));
    assert (framed_len <= worst_framed_len);

    printf ("worst framed  : %10zi\n"
	    "      framed  : %10zi      ratio: %lg\n"
	    "double array  : %10zi      ratio: %lg\n",
	    worst_framed_len, framed_len, ((double)framed_len)/worst_framed_len,
	    M*N*sizeof (double), ((double)framed_len)/(M*N*sizeof (double)));

    dec_unframed_len = mtxdec_unframed_len (framed);
    dec_unframed = malloc (dec_unframed_len);
    assert (mtxdec_unframe (dec_unframed, framed));

    int dec_tag, dec_ndim;
    size_t *dec_dim;

    mtxdec_decode_header (&dec_tag, &dec_ndim, &dec_dim, dec_unframed);
    assert (dec_tag == MTXCDC_REAL_DOUBLE);
    assert (dec_ndim == 2);
    assert (dec_dim[0] == M);
    assert (dec_dim[1] == N);
    mtxdec_decode_data (dec_d, dec_unframed);

    if (0 != memcmp (d, dec_d, M*N*sizeof (double))) {
	for (int k = 0; k < M*N; ++k)
	    printf ("    d[%3d] = %20.15e\n"
		    "dec_d[%3d] = %20.15e\n"
		    "             diff = %20.15e\n",
		    k, d[k], k, dec_d[k], (d[k] - dec_d[k]));
	abort ();
    }

    free (dec_dim);
    free (dec_unframed);
    free (unframed);
    free (framed);
    free (dec_d);
    free (d);
}

static void
test_double_complex (void)
{
    double _Complex *d, *dec_d;
    double *dalias;
    uint8_t *unframed, *framed, *dec_unframed;

    size_t worst_unframed_len, worst_framed_len;
    size_t unframed_len, framed_len, dec_unframed_len;
    d = malloc (M*N*sizeof(double _Complex));
    assert (d);
    dalias = (double*)d;
    for (int k = 0; k < 2*M*N; ++k) dalias[k] = drand48 ();

    dec_d = malloc (M*N*sizeof(double _Complex));
    assert (dec_d);
    dalias = (double*)dec_d;
    for (int k = 0; k < 2*M*N; ++k) dalias[k] = -99.0;

    worst_unframed_len = mtxenc_worst_uncompressed_len (MTXCDC_COMPLEX_DOUBLE, 2, dim);
    assert (worst_unframed_len);
    unframed = malloc (worst_unframed_len);
    assert (unframed);
    assert (mtxenc_pack (unframed, &unframed_len, MTXCDC_COMPLEX_DOUBLE, 2, dim, (void*)d));
    assert (unframed_len <= worst_unframed_len);

    printf ("worst unframed: %10zi\n"
	    "      unframed: %10zi      ratio: %lg\n"
	    "double array  : %10zi      ratio: %lg\n",
	    worst_unframed_len, unframed_len, ((double)unframed_len)/worst_unframed_len,
	    M*N*sizeof (double), ((double)unframed_len)/(M*N*sizeof (double _Complex)));

    worst_framed_len = mtxenc_worst_framed_len (unframed, unframed_len);
    assert (worst_framed_len);
    framed = malloc (worst_framed_len);
    assert (framed);
    assert (mtxenc_frame (framed, &framed_len, unframed, unframed_len));
    assert (framed_len <= worst_framed_len);

    printf ("worst framed  : %10zi\n"
	    "      framed  : %10zi      ratio: %lg\n"
	    "double array  : %10zi      ratio: %lg\n",
	    worst_framed_len, framed_len, ((double)framed_len)/worst_framed_len,
	    M*N*sizeof (double _Complex), ((double)framed_len)/(M*N*sizeof (double _Complex)));

    dec_unframed_len = mtxdec_unframed_len (framed);
    dec_unframed = malloc (dec_unframed_len);
    assert (mtxdec_unframe (dec_unframed, framed));

    int dec_tag, dec_ndim;
    size_t *dec_dim;

    mtxdec_decode_header (&dec_tag, &dec_ndim, &dec_dim, dec_unframed);
    assert (dec_tag == MTXCDC_COMPLEX_DOUBLE);
    assert (dec_ndim == 2);
    assert (dec_dim[0] == M);
    assert (dec_dim[1] == N);
    mtxdec_decode_data (dec_d, dec_unframed);

    if (0 != memcmp (d, dec_d, M*N*sizeof (double _Complex))) {
	for (int k = 0; k < M*N; ++k)
	    printf ("    d[%3d] = (%20.15e, %20.15e)\n"
		    "dec_d[%3d] = (%20.15e, %20.15e)\n"
		    "             diff = %20.15e\n",
		    k, creal(d[k]), cimag(d[k]), k, creal(dec_d[k]), cimag(dec_d[k]),
		    cabs(d[k] - dec_d[k]));
	abort ();
    }

    free (dec_dim);
    free (dec_unframed);
    free (unframed);
    free (framed);
    free (dec_d);
    free (d);
}

int
main (int argc, char **argv)
{
    if (argc > 1) {
	M = N = atoi (argv[1]);
	dim[0] = dim[1] = N;
    }
    if (argc > 2) {
	N = atoi (argv[2]);
	dim[1] = N;
    }
    srand48 (10101ul);

    test_double ();
    test_double_complex ();
}
