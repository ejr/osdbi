// -*- mode: C++; c-file-style: "gnu" -*-
#include <functional>
#include <cctype>

// {{{ Octave headers
#include <octave/config.h>

#include <octave/defun-dld.h>
#include <octave/error.h>
#include <octave/parse.h>

#include <octave/ov.h>
#include <octave/oct-obj.h>
#include <octave/ov-struct.h>
// }}}

#include "osdbi.h"
#include "osdbi-util.h"

// Octave goo:
DEFINE_OCTAVE_ALLOCATOR (octave_osdbi_conn);
DEFINE_OV_TYPEID_FUNCTIONS_AND_DATA (octave_osdbi_conn,
				     "osdbi connection", "osdbi connection");

// {{{ Null implementations of base methods.
namespace osdbi {
  conn_interface::conn_interface () { }
  conn_interface::~conn_interface () { }

  conn_interface::conn_interface (const conn_interface&) {}
  conn_interface& conn_interface::operator= (const conn_interface&) {}
  void conn_interface::swap (conn_interface&) {}

  conn_interface* conn_interface::clone () { return NULL; }

  int conn_interface::set_option (const std::string&,
				  const std::string&) { return 0; }
  int conn_interface::set_option (const std::string&, const int) { return 0; }
  Octave_map conn_interface::get_options () const { return Octave_map(); }

  int conn_interface::open () { return 0; }
  int conn_interface::close () { return 0; }

  bool conn_interface::is_open () const { return false; }
  bool conn_interface::is_readonly () const { return true; };
  bool conn_interface::is_ok () const { return false; }

  std::string conn_interface::driver_name () const { return "NONE"; }
  std::string conn_interface::current_description () const { return "NONE"; }
  std::string conn_interface::current_status () const { return "NONE"; }

  int conn_interface::insert (const std::string&, const Octave_map&,
			      Octave_map*)
  { return 0; }

  int conn_interface::expand (const std::string&, const Octave_map&,
			      Octave_map&, const Octave_map*)
  { return 0; }

  int arbitrary_exec (const std::string&, const Cell&,
		      octave_value_list&)
  { return 0; }
  int conn_interface::arbitrary_exec (const std::string&)
  { return 0; }
}
// }}} namespace osdbi

// {{{ Implementation utilities, not exported.
namespace {
  static bool tracing_flag = false;

  /* Fetch the conn_interface pointer out of various Octave values. */
  osdbi::conn_interface*
  get_conn (const octave_base_value& ov)
  {
    if (ov.type_id () != octave_osdbi_conn::static_type_id ())
      return NULL;
    const octave_osdbi_conn& oconn
      = dynamic_cast<const octave_osdbi_conn&> (ov);
    osdbi::conn_interface* conn = oconn.get_conn ();
    return conn;
  }
  osdbi::conn_interface*
  get_conn (const octave_base_value* ov)
  {
    if (!ov) return NULL;
    return get_conn (*ov);
  }
  osdbi::conn_interface*
  get_conn (octave_value ov)
  { return get_conn (ov.get_rep ()); }

  int
  setopt (osdbi::conn_interface* conn,
	  const std::string& key, const octave_value& valval)
  /* Dispatch the option type. */
  {
    if (valval.is_string ())
      return conn->set_option (key, valval.string_value ());
    else
      return conn->set_option (key, valval.int_value ());
  }

  octave_value
  conn_error (osdbi::conn_interface* conn,
	      int errc)
  /* Gripe. */
  {
    std::string cname = conn->driver_name ();
    std::string status = conn->current_status ();
    error ("Connection type %s returned error code %d, status %s",
	   cname.c_str (), errc, status.c_str ());
    return errc;
  }

  struct apply_tolower
  /* The typical (but not standard) lowercasing operation. */
  { char operator() (char c) const  { return std::tolower(c); } };

}
// }}} anonymous namespace

// {{{ tracing functions implementation ----------------------------
bool
osdbi::is_tracing ()
{ return tracing_flag; }

bool
osdbi::set_tracing (bool newflag)
{
  bool old = tracing_flag;
  tracing_flag = newflag;
  return old;
}

void
osdbi::toggle_tracing ()
{ tracing_flag = !tracing_flag; }
// }}} tracing functions implementation ----------------------------

// {{{ Octave connection value methods ----------------------------
octave_osdbi_conn&
octave_osdbi_conn::operator= (const octave_osdbi_conn& other)
{
  std::auto_ptr<conn_interface> newconn (other.conn->clone ());
  finish ();
  conn = newconn.release ();
}

void
octave_osdbi_conn::print (std::ostream& os, bool pr_as_read_syntax)
  const
{
  print_raw (os, pr_as_read_syntax);
  newline (os);
}

void
octave_osdbi_conn::print_raw (std::ostream& os, bool pr_as_read_syntax)
  const
{
  if (is_defined ())
    os << "osdbi " << conn->driver_name ()
       << " " << conn->current_description ()
       << ", status " << conn->current_status ();
  else
    os << "undefined osdbi connection";
}

void
octave_osdbi_conn::finish ()
{
  if (conn)
    {
      conn->close ();
      delete conn;
      conn = NULL;
    }
}
// }}} Octave connection value methods ----------------------------

// {{{ automutating_column implementation ----------------------------
osdbi::automutating_column::automutating_column () : len (0), type (EMPTY) {}

void
osdbi::automutating_column::ensure_pushable ()
{
  if (EMPTY == type)
    {
      dblary.resize (2);
      int32ary.resize (2);
      int64ary.resize (2);
      cellary.resize (2);
      return;
    }

  octave_idx_type clen;
  switch (type)
    {
    case FLOAT:
      clen = dblary.nelem ();
      if (len+1 >= clen)
	dblary.resize (2*len+1);
      break;
    case INT32:
      clen = int32ary.nelem ();
      if (len+1 >= clen)
	int32ary.resize (2*len+1);
      break;
    case INT64:
      clen = int64ary.nelem ();
      if (len+1 >= clen)
	int64ary.resize (2*len+1);
      break;
    case CELL:
      clen = cellary.nelem ();
      if (len+1 >= clen)
	cellary.resize (2*len+1);
      break;
    }
}

void
osdbi::automutating_column::append (const octave_int32 v)
{
  ensure_pushable ();
  switch (type)
    {
    case EMPTY:
      type = INT32;
    case INT32:
      int32ary (len++) = v;
      return;
    case INT64:
      int64ary (len++) = v;
      return;
    case CELL:
      cellary (len++) = v;
      return;
    case FLOAT:
      dblary (len++) = v;
      return;
    }
}

void
osdbi::automutating_column::append (const octave_int64 v)
{
  ensure_pushable ();
  switch (type)
    {
    case EMPTY:
      type = INT64;
    case INT64:
      int64ary (len++) = v;
      return;
    case INT32:
      int64ary.resize (2*len+1);
      for (octave_idx_type k = 0; k < len; ++k)
	int64ary (k) = int32ary (k);
      int32ary.resize (0);
      type = INT64;
      return;
    case CELL:
      cellary (len++) = v;
      return;
    case FLOAT:
      warning ("Appending a large integer to a float column.");
      dblary (len++) = v;
      return;
    }
}

void
osdbi::automutating_column::append (const double v)
{
  ensure_pushable ();
  switch (type)
    {
    case EMPTY:
      type = FLOAT;
    case FLOAT:
      dblary (len++) = v;
      return;
    case CELL:
      cellary (len++) = v;
      return;
    case INT32:
      dblary.resize (2*len+1);
      for (int k = 0; k < len; ++k)
	dblary (k) = int32ary (k);
      dblary (len++) = v;
      int32ary.resize (0);
      type = FLOAT;
      return;
    case INT64:
      warning ("Converting a large integer column to floats.");
      dblary.resize (2*len+1);
      for (int k = 0; k < len; ++k)
	dblary (k) = int64ary (k);
      dblary (len++) = v;
      int64ary.resize (0);
      type = FLOAT;
      return;
    }
}

void
osdbi::automutating_column::to_cell ()
{
  if (EMPTY == type)
    type = CELL;
  if (CELL == type)
    {
      ensure_pushable ();
      return;
    }

  cellary.resize (2*len+1);

  if (FLOAT == type)
    {
      for (int k = 0; k < len; ++k)
	cellary (k) = dblary (k);
      dblary.resize (0);
    }
  else if (INT32 == type)
    {
      for (int k = 0; k < len; ++k)
	cellary (k) = int32ary (k);
      int32ary.resize (0);
    }
  else
    {
      for (int k = 0; k < len; ++k)
	cellary (k) = int64ary (k);
      int64ary.resize (0);
    }

  type = CELL;
}

void
osdbi::automutating_column::append (const Cell& v)
{
  to_cell ();
  cellary (len++) = v;
}

void
osdbi::automutating_column::append (const std::string& v)
{
  to_cell ();
  cellary (len++) = v;
}

void
osdbi::automutating_column::append (const octave_value& v)
{
  int vtid = v.type_id ();
  if (v.is_string ())
    {
      append (v.string_value ());
      return;
    }
  if (v.is_integer_type ())
    {
      if (vtid == octave_int64_scalar::static_type_id ()
	  || vtid == octave_uint32_scalar::static_type_id ())
	{
	  append (v.int64_scalar_value ());
	  return;
	}
      else if (vtid != octave_uint64_scalar::static_type_id ())
	{
	  append (v.int32_scalar_value ());
	  return;
	}
    }
  if (v.is_scalar_type () && !v.is_complex_type ())
    {
      append (v.double_value ());
      return;
    }
  to_cell ();
  cellary (len++) = v;
}

void
osdbi::automutating_column::append ()
{
  ensure_pushable ();
  append (octave_NA);
}

octave_value
osdbi::automutating_column::get_octave_value ()
{
  switch (type)
    {
    case CELL:
      cellary.resize (len);
      return octave_value (cellary);
    case INT32:
      int32ary.resize (len);
      return octave_value (int32ary);
    case INT64:
      int64ary.resize (len);
      return octave_value (int64ary);
    default:
      dblary.resize (len);
      return octave_value (dblary);
    }
}
// }}} automutating_column implementation ----------------------------

// {{{ Octave-level functions ----------------------------

// PKG_ADD: autoload ("osdbi", "osdbi.oct");
// PKG_ADD: mlock ();
DEFUN_DLD (osdbi, args, ,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{conn} = } osdbi (@var{drivername}[, @var{key}, @var{val}, ...])\n\
@deftypefnx {Loadable Function} {@var{conn} = } osdbi (@var{toclone})\n\
Make a new connection through driver @var{drivername}.  Parameters\n\
can be passed by optional @var{key} strings and @var{val} values.\n\
If given an existing connection, the returned @var{conn} is a new,\n\
cloned connection to the original data source if possible.\n\
@example\n\
@group\n\
@code{db = osdbi(\"sqlite3\")}\n\
@result{} osdbi SQLite3 in-memory database <0>, status closed\n\
@end group\n\
@group\n\
@code{db = osdbi(\"SQLite3\", \"filename\", \"tst.sqlite3\")}\n\
@result{} osdbi SQLite3 connection to tst.sqlite3 <24849672>, status closed\n\
@end group\n\
@end example\n\
@end deftypefn")
{
  const int nargin = args.length ();

  if (nargin < 1 || nargin % 2 != 1)
    {
      print_usage ();
      return octave_value ();
    }

  osdbi::conn_interface *conn = get_conn (args (0));
  if (conn) return octave_value (conn->clone ());

  std::string drivername = args (0).string_value ();
  std::transform (drivername.begin (), drivername.end (),
		  drivername.begin (), apply_tolower () );
  std::string fname = "osdbi_" + drivername + "_make";
  octave_value_list tmp;
  tmp = feval (fname, octave_value_list(), 1);
  octave_value octconn = tmp(0);

  conn = get_conn (octconn);
  if (!conn) { error ("Cannot get connection..."); return octconn; }

  for (int k = 1; k < nargin; k += 2)
    {
      const std::string key = args (k).string_value ();
      const int errc = setopt (conn, key, args (k+1));
      if (errc) return conn_error (conn, errc);
    }

  return octconn;
}

// PKG_ADD: autoload ("osdbi_set_option", "osdbi.oct");
DEFUN_DLD (osdbi_set_option, args, ,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{errcode} =} osdbi_set_option (@var{conn}, @var{key}, @var{val}, ...)\n\
@deftypefnx {Loadable Function} {@var{errcode} =} osdbi_set_option (@var{conn}, @var{optstruct})\n\
Set options on connection @var{conn}.  The returned @var{errcode} is zero on\n\
success.  Other values are driver-dependent.\n\
@example\n\
@code{db = osdbi (\"sqlite3\");}\n\
@code{osdbi_set_option (db, \"filename\", \"tst.sqlite3\");}\n\
@code{osdbi_set_option (db, struct (\"flags\", 8));}\n\
@end example\n\
@seealso{osdbi}\n\
@end deftypefn")
{
  //using namespace osdbi;
  const int nargin = args.length ();

  if (nargin < 1 || (nargin > 2 && nargin%2 != 1))
    {
      print_usage ();
      return octave_value (-(nargin+1));
    }
  osdbi::conn_interface* conn = get_conn (args (0));
  if (!conn)
    {
      error("First argument must be a osdbi connection.");
      return octave_value (-1);
    }
  if (nargin == 2)
    {
      if (args (1).type_id () != octave_struct::static_type_id ()
	  || args (1).numel() != 1)
	{
	  error("If only two arguments are provided, "
		"the second must be a structure of scalar options.");
	  return octave_value (-2);
	}
    }

  octave_value success(0);

  if (nargin == 2)
    {
      const Octave_map keyval = args (1).map_value ();
      Octave_map::const_iterator it;
      const Octave_map::const_iterator done = keyval.end();
      for (it = keyval.begin(); it != done; ++it)
	{
	  const std::string key = keyval.key (it);
	  const Cell& valcell = keyval.contents (it);
	  const octave_value& valval = valcell (0);
	  const int errc = setopt (conn, key, valval);
	  if (errc) return conn_error (conn, errc);
	}
      return success;
    }

  // Have (conn, key, val, key, val, ...)
  for (int k = 1; k < nargin; k += 2) {
    const std::string key = args (k).string_value ();
    const int errc = setopt (conn, key, args (k+1));
    if (errc) return conn_error (conn, errc);
  }
  return success;
}

// PKG_ADD: autoload ("osdbi_open", "osdbi.oct");
DEFUN_DLD (osdbi_open, args, ,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{errcode} =} osdbi_open (@var{conn})\n\
Opens the connection.  A zero @var{errcode} signals success; other values\n\
are driver-dependent.\n\
@seealso{osdbi}\n\
@end deftypefn")
{
  using namespace osdbi;
  octave_value success (0);
  const int nargin = args.length ();

  if (nargin != 1)
    {
      print_usage ();
      return octave_value(-(nargin+1));
    }
  conn_interface* conn = get_conn (args (0));
  if (!conn)
    {
      error("First argument must be a osdbi connection.");
      return octave_value(-1);
    }
  const int errc = conn->open ();
  if (errc) return conn_error (conn, errc);
  return success;
}

// PKG_ADD: autoload ("osdbi_close", "osdbi.oct");
DEFUN_DLD (osdbi_close, args, ,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {} osdbi_close (@var{conn})\n\
Closes the connection.\n\
@seealso{osdbi}\n\
@end deftypefn")
{
  using namespace osdbi;
  octave_value success (0);
  const int nargin = args.length ();

  if (nargin != 1)
    {
      print_usage ();
      return octave_value(-(nargin+1));
    }
  conn_interface* conn = get_conn (args (0));
  if (!conn)
    {
      error("First argument must be a osdbi connection.");
      return octave_value(-1);
    }
  const int errc = conn->close ();
  if (errc) return conn_error (conn, errc);
  return octave_value ();
}

namespace {
  template <typename Func>
  octave_value simple_getter (const octave_value_list& args, Func func)
  {
    const int nargin = args.length ();

    if (nargin != 1)
      {
	print_usage ();
	return octave_value ();
      }
    osdbi::conn_interface* conn = get_conn (args (0));
    if (!conn)
      {
	error("First argument must be a osdbi connection.");
	return octave_value ();
      }
    return octave_value (func (*conn));
  }
} // anon namespace

// PKG_ADD: autoload ("osdbi_is_open", "osdbi.oct");
DEFUN_DLD (osdbi_is_open, args, ,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {} osdbi_is_open (@var{conn})\n\
Returns true if the connection is open.\n\
@seealso{osdbi}\n\
@end deftypefn")
{
  return simple_getter (args,
			std::mem_fun_ref (&osdbi::conn_interface::is_open));
}

// PKG_ADD: autoload ("osdbi_is_readonly", "osdbi.oct");
DEFUN_DLD (osdbi_is_readonly, args, ,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {} osdbi_is_readonly (@var{conn})\n\
Returns true if the connection is read-only.\n\
@seealso{osdbi}\n\
@end deftypefn")
{
  return simple_getter (args,
			std::mem_fun_ref (&osdbi::conn_interface::is_readonly));
}

// PKG_ADD: autoload ("osdbi_is_ok", "osdbi.oct");
DEFUN_DLD (osdbi_is_ok, args, ,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {} osdbi_is_ok (@var{conn})\n\
Returns true if the connection is in a good (non-error) state.\n\
The exact meaning is driver-dependent, but it is reasonable to assume\n\
that a non-\"ok\" connection should be closed.  It may be possible to\n\
generate an \"ok\" connection by cloning the non-\"ok\" connection.\n\
@seealso{osdbi}\n\
@end deftypefn")
{
  return simple_getter (args,
			std::mem_fun_ref (&osdbi::conn_interface::is_ok));
}

// PKG_ADD: autoload ("osdbi_driver_name", "osdbi.oct");
DEFUN_DLD (osdbi_driver_name, args, ,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {} osdbi_driver_name (@var{conn})\n\
Returns the driver name.\n\
@seealso{osdbi}\n\
@end deftypefn")
{
  return simple_getter (args,
			std::mem_fun_ref (&osdbi::conn_interface::driver_name));
}

// PKG_ADD: autoload ("osdbi_current_description", "osdbi.oct");
DEFUN_DLD (osdbi_current_description, args, ,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {} osdbi_current_description (@var{conn})\n\
Returns a string containing a driver-dependent description of the\n\
connection intended for end-user consumption.\n\
@seealso{osdbi}\n\
@end deftypefn")
{
  return simple_getter (args,
			std::mem_fun_ref (&osdbi::conn_interface::current_description));
}

// PKG_ADD: autoload ("osdbi_current_status", "osdbi.oct");
DEFUN_DLD (osdbi_current_status, args, ,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {} osdbi_current_status (@var{conn})\n\
Returns a string containing a driver-dependent description of the\n\
connection intended for end-user consumption.\n\
@seealso{osdbi}\n\
@end deftypefn")
{
  return simple_getter (args,
			std::mem_fun_ref (&osdbi::conn_interface::current_status));
}

namespace {
  void
  setup_outdata (Octave_map& out, const octave_value arg)
  {
    if (arg.is_string ())
      out.assign (arg.string_value (), octave_value ());
    else if (arg.is_cell ())
      {
	Cell tmp (arg.cell_value ());
	const octave_idx_type nf = tmp.nelem ();
	for (octave_idx_type i = 0; i < nf; ++i)
	  out.assign (tmp.elem (i).string_value (), octave_value ());
      }
    else
      out = arg.map_value ();
  }
} // anonymous namespace

// PKG_ADD: autoload ("osdbi_insert", "osdbi.oct");
DEFUN_DLD (osdbi_insert, args, nargout,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{errcode} =} osdbi_insert (@var{conn}, @var{tablename}, @var{data})\n\
@deftypefnx {Loadable Function} {@var{outdata} =} osdbi_insert (@var{conn}, @var{tablename}, @var{data}[, @var{outfields}])\n\
@deftypefnx {Loadable Function} {@var{outdata} =} osdbi_insert (@var{conn}, @var{tablename}, @var{data}[, @var{outname}, ...])\n\
Insert scalar structure @var{data} into table @var{tablename} on connection\n\
@var{conn}.  The fieldnames are used as column names.  Each field should\n\
contain an array or cell array of values.  If a field is scalar, it is\n\
replicated to match any array fields.  A cell array may contain one level\n\
of cell arrays below it; the enclosed cells must hold arrays.  An empty\n\
field is treated as a null.  All array fields must be of the same length.\n\
The returned @var{errcode} will be zero on success.  Data is inserted in a\n\
single transaction, so either all the data is inserted or none is.\n\
\n\
The optional structure @var{outfields} or list of strings @var{outname} specify\n\
fields to return from the recently inserted data.  These can be used to\n\
retrieve autoincrement id fields or other generated key data.\n\
\n\
On success the return value will depend on the number of requested columns\n\
and their types.  Multiple columns will be returned in a structure.  A single\n\
column will be returned as either a Cell column or, if possible, downcasted\n\
to a numerical column which may in turn be narrowed to a scalar.\n\
\n\
On error, the return value will be a scalar error code which may be\n\
indistinguishable from actual data.  However, an error is signaled through\n\
the common Octave mechanism.  A try block or an eval is recommended to detect\n\
errors.\n\
@example\n\
@group\n\
@code{osdbi_do (zz, \"create table aa (a, b);\");}\n\
@code{indata = struct (\"a\", int32([1,2,3,4]), \"b\", 5:8);}\n\
@code{osdbi_insert (zz, \"aa\", indata)}\n\
@result{} 0\n\
@code{indata = struct (\"a\", @{@{1:3, [1,2,3,4]@}@}, \"b\", @{@{ones(5), randn(2)@}@});}\n\
@code{osdbi_insert (zz, \"aa\", indata)}\n\
@result{} 0\n\
@code{indata = struct (\"a\", @{@{\"foo\", \"quux\"@}@}, \"b\", @{@{\"bar\", \"baz\"@}@});}\n\
@code{osdbi_insert (zz, \"aa\", indata, \"a\")}\n\
@result{} @{\n\
  a =\n\
  @{\n\
    [1,1] = foo\n\
    [2,1] = quux\n\
  @}\n\
@}\n\
@end group\n\
@end example\n\
@seealso{osdbi, osdbi_expand}\n\
@end deftypefn")
{
  using namespace osdbi;
  const int nargin = args.length ();

  if (nargin < 3)
    {
      print_usage ();
      return octave_value ();
    }

  conn_interface* conn = get_conn ( args (0));
  if (!conn)
    {
      error ("First argument must be a osdbi connection.");
      return octave_value ();
    }

  if (!args (1).is_string ())
    {
      error ("Second argument must be a string for a table name.");
      return octave_value ();
    }
  std::string table_name = args (1).string_value ();

  if (args (2).type_id () != octave_struct::static_type_id ()
      || args (2).numel () == 0)
    {
      error ("Third argument must be a non-empty structure to insert.");
      return octave_value ();
    }
  Octave_map data = args (2).map_value ();

  const bool return_rowids = nargout > 0;

  Octave_map out;
  bool do_fetch = false;

  if (nargin >= 4)
    {
      do_fetch = true;
      setup_outdata (out, args (3));
      for (int k = 4; k < nargin; ++k)
	  if (args (k).is_string ())
	    out.assign (args (k).string_value (), octave_value ());
	  else
	    {
	      print_usage ();
	      return octave_value ();
	    }
    }

  int errc = conn->insert (table_name, data, (do_fetch? &out : NULL));
  if (errc) return conn_error (conn, errc);
  if (do_fetch) {
    if (out.nfields () < 1) return octave_value ();
    if (out.nfields () > 1) return octave_value (out);
    Cell& contents = out.contents (out.begin ());
    if (contents.nelem () < 1) return octave_value ();
    if (contents.nelem () > 1) return octave_value (contents);
    return contents (0);
  }
  return octave_value ();
}

// PKG_ADD: autoload ("osdbi_do", "osdbi.oct");
DEFUN_DLD (osdbi_do, args, nargout,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {outdata =} osdbi_do (@var{conn}, @var{sql}[, @var{params}...])\n\
@deftypefnx {Loadable Function} {outdata =} osdbi_do (@var{conn}, @var{sql}[, @var{cell_of_params}])\n\
An arbitrary superfunction.  Parameters are bound by position.  Results are\n\
narrowed when possible.\n\
@example\n\
@group\n\
@code{# Assume table tbl has two columns, a and b, and eight entries.}\n\
@code{osdbi_do (conn, \"select count(*) on tbl;\")}\n\
@result{} 8\n\
@end group\n\
@group\n\
@code{osdbi_do (conn, \"select * on tbl limit 1;\")}\n\
@result{} ans =\n\
@{\n\
  a =  22\n\
  b =  bar\n\
@}\n\
@end group\n\
@group\n\
@code{[x, y] = osdbi_do (conn, \"select * on tbl limit 1;\")}\n\
@result{} x =  22\n\
y =  bar\n\
@end group\n\
@end example\n\
@seealso{osdbi, osdbi_insert, osdbi_expand}\n\
@end deftypefn")
{
  using namespace osdbi;
  const int nargin = args.length ();

  if (nargin < 2)
    {
      print_usage ();
      return octave_value ();
    }

  conn_interface* conn = get_conn ( args (0));
  if (!conn)
    {
      error ("First argument must be a osdbi connection.");
      return octave_value ();
    }
  std::string cmd (args (1).string_value ());

  if (error_state) return octave_value ();

  Cell params;
  if (nargin >= 3)
    {
      if (args (3).is_cell ())
	{
	  params = args (3);
	  if (nargin > 3) warning ("Ignoring excess arguments.");
	}
      else
	params = Cell (args.splice (0, 3));
    }
  octave_value_list outvals;

  if (error_state) return octave_value ();

  int errc = conn->arbitrary_exec (cmd, params, outvals);
  octave_idx_type ol = outvals.length ();
  if (errc || error_state || !ol)
    return octave_value ();

  if (nargout <= 1)
    {
      if (ol == 1)
	return outvals;
      Octave_map out;
      string_vector nm = outvals.name_tags ();
      for (int k = 0; k < ol; ++k)
	out.assign (nm (k), outvals (k));
      return octave_value (out);
    }
  if (nargout != ol)
    warning ("Incorrect number of receiving variables.");
  return outvals;
}

// PKG_ADD: autoload ("osdbi_expand", "osdbi.oct");
DEFUN_DLD (osdbi_expand, args, nargout,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{data} =} osdbi_expand (@var{conn}, @var{table}, @var{indata}[, @var{outfields}][, ':NOT', @var{neq}])\n\
@deftypefnx {Loadable Function} {@var{data} =} osdbi_expand (@var{conn}, @var{table}, @var{indata}[, @var{outname1}, ...][, ':NOT', @var{neq})\n\
Expands the structure array @var{indata} with data from table @var{table} through\n\
DB connection @var{conn}.  Each structure array entry provides a pattern of\n\
values to match to values in @var{table}.  The fieldnames are used as column\n\
names.  The result is the distinct union of all the matching rows.  Empty\n\
fields are wildcards as is an empty (or non-existent) @var{indata} argument.\n\
The optional @var{neq} structure provides a set of not-equal constraints\n\
which remove any matches of the database table that match @var{neq}.  The\n\
removal occurs before checking against @var{indata}.\n\
\n\
Output fields can be limited either by the fieldnames of @var{outfields} or\n\
by string names.  The result is a structure on success and either a scalar\n\
error code or an empty matrix on failure.\n\
\n\
Currently NULLs are translated to Octave's NA.\n\
@example\n\
@group\n\
@code{q2 = struct (\"a\", @{@{-100, \"foo\"@}@});}\n\
@code{osdbi_expand (zz, \"aa\", q2)}\n\
@result{}@{\n\
  a = @{\n\
    [1,1] = -100\n\
    [2,1] = foo\n\
  @}\n\
  b = @{\n\
    [1,1] = NA\n\
    [2,1] = bar\n\
  @}\n\
@}\n\
@end group\n\
@end example\n\
@seealso{osdbi, osdbi_insert}\n\
@end deftypefn")
{
  using namespace osdbi;
  const int nargin = args.length ();

  if (nargin < 2)
    {
      print_usage ();
      return octave_value ();
    }

  conn_interface* conn = get_conn (args (0));
  if (!conn)
    {
      error ("First argument must be a osdbi connection.");
      return octave_value ();
    }
  std::string tname (args (1).string_value ());
  Octave_map in;
  Octave_map out;
  Octave_map neq;
  int start_outarg = 2;
  int after_outarg = nargin;

  if (nargin > 2 && args (2).is_map ())
    {
      in = args (2).map_value ();
      start_outarg = 3;
    }

  if (nargin - 2 >= 3)
    {
      if (args (nargin-2).is_string () && args (nargin-1).is_map ())
	{
	  std::string s = args (nargin-2).string_value ();
	  std::transform (s.begin (), s.end (), s.begin (),
			  apply_tolower ());
	  if (s == ":not") {
	    neq = args (nargin-1).map_value ();
	    after_outarg = nargin-2;
	  }
	}
    }

  if (after_outarg > start_outarg)
    {
      setup_outdata (out, args (start_outarg));
      for (int k = start_outarg+1; k < after_outarg; ++k)
	  if (args (k).is_string ())
	    out.assign (args (k).string_value (), octave_value ());
	  else
	    {
	      print_usage ();
	      return octave_value ();
	    }
    }

  if (!error_state)
    {
      int errc = conn->expand (tname, in, out, (neq.nfields ()? &neq : 0));
      if (!errc) return octave_value (out);
      return conn_error (conn, errc);
    }
  else
    return octave_value ();
}

// PKG_ADD: autoload ("osdbi_toggle_tracing", "osdbi.oct");
DEFUN_DLD (osdbi_toggle_tracing, , ,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {} osdbi_toggle_tracing ()\n \
Turn on tracing.  The exact effects depend on the driver.\n\
@end deftypefn")
{
  osdbi::toggle_tracing ();
  return octave_value ();
}

// PKG_ADD: autoload ("osdbi_is_tracing", "osdbi.oct");
DEFUN_DLD (osdbi_is_tracing, , ,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {} osdbi_is_tracing ()\n \
Returns true if tracing.  The exact effects of tracing depend on the driver.\n\
@end deftypefn")
{ return octave_value (osdbi::is_tracing ()); }

// }}} Octave level functions ----------------------------
