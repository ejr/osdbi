#include <sqlite3.h>

#include <octave/config.h>
#include <octave/ov.h>
#include <octave/oct-obj.h>
#include <octave/defun-dld.h>

DEFUN_DLD (tstsqlite, args, , "")
{
    sqlite3 *db;
    sqlite3_open_v2 (":memory:", &db,
		     SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
    sqlite3_close (db);
    return octave_value(true);
}
