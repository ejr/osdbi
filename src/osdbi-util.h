// -*- mode: C++; c-file-style: "gnu" -*-
#ifndef MYSDBI_UTIL_HEADER_
#define MYSDBI_UTIL_HEADER_ 1

#include <limits>

#include <octave/config.h>
#include <octave/dNDArray.h>
#include <octave/int64NDArray.h>
#include <octave/Cell.h>

#include <octave/ov-uint64.h>
#include <octave/ov-int64.h>
#include <octave/ov-uint32.h>

namespace osdbi
{
  /* osdbi's supported column types.  Cells can contain any of the
     listed scalar types as well as strings and blobs (treated as
     uint8 vectors). */
  typedef enum { INT32, INT64, FLOAT, CELL, EMPTY } coltype_t;

  struct automutating_column
  /* Collects result columns and handles type conversions as values
     are added.  Some database systems (e.g. SQLite) provide dynamic
     typing within a column, and other types may not precisely match
     Octave's types.  So osdbi does not rely on the declared column
     types but rather extends the column's type as needed.

     The type conversions are as follows:

     |   | current: | empty  | int32  | int64   | double | cell |
     |---+----------+--------+--------+---------+--------+------|
     | i | int<=32  | int32  | int32  | int64   | double | cell |
     | n | int<=64  | int64  | int64  | int64   | double | cell |
     | s | double   | double | double | double* | double | cell |
     | e | string   | cell   | cell   | cell    | cell   | cell |
     | r | blob     | cell   | cell   | cell    | cell   | cell |
     | t | null     | double | double | double* | double | cell |

     The conversion of an int64 column to double can lose information,
     so the starred choice may be controversial.  Another possibly
     different choice is letting NULL entries become octave_NA even in
     a Cell column; they could be stored as empty matrices.
  */
  {
    octave_idx_type len;
    coltype_t type;

    Cell cellary;
    int32NDArray int32ary;
    int64NDArray int64ary;
    NDArray dblary;

    automutating_column ();

    /* Push the given value, copying if necessary. */
    void append (const octave_int32 v);
    void append (const octave_int64 v);
    void append (const double v);
    void append (const Cell& v);
    void append (const std::string& v);
    void append (); // push a NULL
    /* The following is a catch-all intended for blobs, but it can be
       abused to handle other types. */
    void append (const octave_value& v);

    /* Return the column. */
    octave_value get_octave_value ();

    /* Mostly internal, but possibly useful to drivers. */
    /* Ensure there's room to push an entry without reallocating. */
    void ensure_pushable ();
    /* Convert the column to a Cell column. */
    void to_cell ();
  };
} // namespace osdbi

#endif /* MYSDBI_UTIL_HEADER_ */
