// -*- mode: C++; c-file-style: "gnu" -*-
#include <cstring>
#include <functional>
#include <cctype>
#include <cstring>
#include <string>
#include <sstream>
#include <vector>
#include <cassert>
#include <complex>

#include <iostream>

#include <sqlite3.h>

// {{{ Octave headers
#include <octave/config.h>

#include <octave/defun-dld.h>
#include <octave/error.h>

#include <octave/ov.h>
#include <octave/oct-obj.h>
#include <octave/ov-struct.h>
#include <octave/ov-cell.h>
#include <octave/ov-scalar.h>
#include <octave/ov-bool.h>
#include <octave/ov-uint8.h>
#include <octave/ov-int8.h>
#include <octave/ov-int16.h>
#include <octave/ov-uint16.h>
#include <octave/ov-int32.h>
#include <octave/ov-uint32.h>
#include <octave/ov-int64.h>
#include <octave/ov-uint64.h>
#include <octave/ov-re-mat.h>
#include <octave/ov-cx-mat.h>
#include <octave/ov-range.h>
// }}}

#define DOUBLE_COMPLEX std::complex<double>
#define FLOAT_COMPLEX std::complex<float>
#define MTXCDC_EMBED static
#include "mtxcodec/mtxcodec.h"
#include "mtxcodec/mtxcodec.c"

#include "osdbi.h"
#include "osdbi-util.h"

// {{{ Break handling
extern "C" {
  static int checkbreak (void*)
  /* Used for clean handling of ^C in SQLite calls. */
  {
    if (octave_signal_caught) return 1;
    return 0;
  }
}

/* How many SQLite bytecode operations between checks for ^C? */
static int default_nops = 1000; // XXX: need some way to change this per conn?
// }}}

namespace {
  /*
    Only the Octave functions are exported from this file.  All
    non-DEFUN_DLD symbols should not be visible.
  */

  // {{{ SQLite3 connection implementation
  struct tmpdbptr
  /* Safely hold a sqlite3* temporarily in the connection's operator=. */
  {
    sqlite3* db;
    tmpdbptr () : db (NULL) { }
    tmpdbptr (sqlite3* db) : db (db) { }
    ~tmpdbptr () { if (db) sqlite3_close (db); }
    sqlite3* release () { sqlite3* out = db; db = NULL; return out; }
  };

  sqlite3*
  do_open (const std::string& fname, int flags)
  /* Open an SQLite3 db in file fname with the given flags.  If fname
     is empty, use the magic ":memory:" name for an in-memory db. */
  {
    sqlite3* out;
    int errc;
    if (fname.empty ())
      errc = sqlite3_open_v2 (":memory:", &out, flags, NULL);
    else
      errc = sqlite3_open_v2 (fname.c_str (), &out, flags, NULL);
    if (errc != SQLITE_OK)
      throw errc;
    sqlite3_extended_result_codes (out, 1);
    sqlite3_progress_handler (out, default_nops, checkbreak, NULL);
    return out;
  }

  int
  do_arb_exec (sqlite3* db, const std::string& q, bool dothrow = true)
  /* Helper for executing an arbitrary query in q.  On error, returns
     or throws the integer error code repored from SQLite3. */
  {
    if (osdbi::is_tracing ())
      std::cerr << "DO in " << uintptr_t(db) << " : " << q << std::endl;
    int errc = sqlite3_exec (db, q.c_str (), NULL, NULL, NULL);
    if (dothrow && errc)
      throw errc;
    return errc;
  }

  class sqlite3_conn : public osdbi::conn_interface
  /* The SQLite3 connection interface.  There is no additional
     functionality beyond osdbi::conn_interface, so the definition is
     purely internal. */
  {
  protected:
    sqlite3* db;
    std::string filename;
    int flags;

    int lasterrc;
    std::string lasterr;

    /* do_errc handles the different error reporting styles,
       setting the error message according either to the provided
       string or to SQLite3's errmsg. */
    int do_errc (int errc)
    {
      lasterrc = errc;
      if (errc < 0)
	lasterr = "Interrupted.";
      else if (errc != SQLITE_OK)
	lasterr = sqlite3_errmsg (db);
      return errc;
    }
    int do_errc (int errc, const std::string& msg)
    {
      lasterrc = errc;
      lasterr = msg;
      return errc;
    }
    int do_errc (int errc, const char* msg)
    {
      lasterrc = errc;
      if (msg) lasterr = msg;
      return errc;
    }

  public:
    sqlite3_conn () : db (NULL), filename (),
		      flags (SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE),
		      lasterrc (0), lasterr ()
    { }
    ~sqlite3_conn () { if (is_open ()) close(); }

    sqlite3_conn (const sqlite3_conn& other)
      : conn_interface (other), filename (other.filename), flags (other.flags),
	lasterrc (0), lasterr ()
    { if (other.is_open ()) open (); }

    sqlite3_conn& operator= (const sqlite3_conn& other)
    /* Exception-safe, leaves *this unchanged on error. */
    {
      std::string new_fname = other.filename;

      tmpdbptr new_db (do_open (new_fname, other.flags));
      conn_interface::operator= (other);
      if (is_open ()) close ();

      std::swap (filename, new_fname);
      flags = other.flags;
      db = new_db.release ();

      return *this;
    }

    sqlite3_conn* clone ()
    {
      sqlite3_conn* out = new sqlite3_conn (*this);
      return out;
    }

    int set_option (const std::string& key,
		    const std::string& val)
    /* Accepted string options:

       | Key       | Value                                            |
       |-----------+--------------------------------------------------|
       | filename, | File to open, can me "" or ":memory:"            |
       | name      |                                                  |
       |-----------+--------------------------------------------------|
       | flags,    | Accepts "read-only", "read-write", and "create", |
       | flag      | plus a few common typos.                         |
    */
    {
      if (is_open ())
	return do_errc (SQLITE_MISUSE,
			"Cannot set options on an open connection.");
      if (key == "filename" || key == "name")
	{
	  filename = val;
	  return do_errc (SQLITE_OK);
	}
      if (key == "flags" || key == "flag")
	{
	  if (val == "readonly" || val == "read-only")
	    flags = SQLITE_OPEN_READONLY;
	  else if (val == "writable" || val == "readwrite"
		   || val == "read-write" || val == "read/write")
	    flags = SQLITE_OPEN_READWRITE;
	  else if (val == "create")
	    flags |= SQLITE_OPEN_CREATE;
	  else
	    return do_errc (SQLITE_MISUSE, "Unrecognized flag string.");
	  return do_errc (SQLITE_OK);
	}
      return do_errc (SQLITE_MISUSE, "Unrecognized option.");
    }

    int set_option (const std::string& key,
		    const int val)
    /* Accepts numerical values for "flags".  However, the connection
       interface does not provide a mechanism to return the library's
       numerical codes.  Use of string values is preferred, but ints
       are stored in the option structure. */
    {
      if (is_open ())
	return do_errc (SQLITE_MISUSE,
			"Cannot set options on an open connection.");
      if (key == "flags" || key == "flag")
	{
	  switch (val)
	    {
	    case SQLITE_OPEN_READONLY:
	    case SQLITE_OPEN_READWRITE:
	    case SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE:
	      flags = val;
	      return do_errc (SQLITE_OK);
	    default:
	      return do_errc (SQLITE_MISUSE, "Unrecognized flag integer.");
	    }
	}
      return do_errc (SQLITE_MISUSE,  "Unrecognized option.");
    }

    Octave_map get_options () const
    {
      Octave_map out;
      out.assign ("filename", filename);
      out.assign ("flags", flags);
      return out;
    }

    int open ()
    {
      if (is_open ())
	return do_errc (SQLITE_MISUSE, "Already open.");
      try
	{
	  db = do_open (filename, flags);
	}
      catch (int errc)
	{
	  return do_errc (errc, "Open failed.");
	}
      return SQLITE_OK;
    }
    int close ()
    {
      if (!is_open ()) return SQLITE_OK;
      int errc = sqlite3_close (db);
      db = NULL;
    }

    bool is_open () const { return db != NULL; }
    bool is_readonly () const { return flags == SQLITE_OPEN_READONLY; }
    bool is_ok () const { return lasterrc == SQLITE_OK; }

    std::string driver_name () const { return "SQLite3"; }
    std::string current_description () const
    {
      std::ostringstream os;
      if (filename.empty () || filename == ":memory:")
	os << "in-memory database";
      else
	os << "connection to " << filename;
      os << " <" << ((intptr_t)db) << ">";
      return os.str ();
    }
    std::string current_status () const
    {
      std::ostringstream os;
      if (!is_open ())
	os << "closed";
      else
	os << "open";
      /* XXX: I don't have this quite right...  Sometimes error codes
	 are reported that have nothing to do with the message. */
      if (!is_ok ())
	os << " error " << lasterrc << ": " << lasterr;
      return os.str ();
    }

    int insert (const std::string& /* table name */,
		const Octave_map& /* data */,
		Octave_map*);

    /* expand is essentially an inner join. */
    int expand (const std::string& /* table name */,
		const Octave_map& /* input field values */,
		Octave_map& /* output */,
		const Octave_map* /* not= constraints */);

    int arbitrary_exec (const std::string&, const Cell&,
			octave_value_list&);
    int arbitrary_exec (const std::string& qstring)
    { return do_arb_exec( db, qstring, false); }
  };
  // }}} SQLite3 connection implementation

  // {{{ SQLite3 values <-> Octave values

  int blob_handles (int tid)
  {
    if (tid == octave_uint8_matrix::static_type_id ()) return MTXCDC_UINT8;

    if (tid == octave_int8_matrix::static_type_id ()) return MTXCDC_INT32;
    if (tid == octave_int16_matrix::static_type_id ()) return MTXCDC_INT32;
    if (tid == octave_uint16_matrix::static_type_id ()) return MTXCDC_INT32;
    if (tid == octave_int32_matrix::static_type_id ()) return MTXCDC_INT32;

    if (tid == octave_range::static_type_id ()) return MTXCDC_REAL_DOUBLE;
    if (tid == octave_matrix::static_type_id ()) return MTXCDC_REAL_DOUBLE;
    if (tid == octave_complex_matrix::static_type_id ()) return MTXCDC_COMPLEX_DOUBLE;

    return 0;
  }

  struct blob_ov {
    std::vector<uint8_t> encblob;

    void* fortran_vec () { return static_cast<void*> (&encblob[0]); }
    const void* fortran_vec () const { return static_cast<const void*> (&encblob[0]); }
    octave_idx_type numel () const { return encblob.size (); }

    void encode (const Cell& cell)
    {
      const octave_value& val = cell (0);
      if (val.type_id() == octave_cell::static_type_id ())
	throw std::string ("Will not strip multiple levels of cell encapsulation.");
      encode (val);
    }

    void encode (const octave_value& val)
    {
      if (val.type_id () == octave_cell::static_type_id ()){
	encode (val.cell_value ());
	return;
      }
      int cdctype = blob_handles (val.type_id ());
      if (!cdctype)
	throw std::string ("Unconvertable type: " + val.type_name ());
      switch (cdctype) {
      case MTXCDC_REAL_DOUBLE:
	encode (val.array_value ());
	break;
      case MTXCDC_COMPLEX_DOUBLE:
	encode (val.complex_array_value ());
	break;
      case MTXCDC_INT32:
	encode (val.int32_array_value ());
	break;
      case MTXCDC_UINT8:
	encode (val.uint8_array_value ());
	break;
      }
    }

    bool do_encode (const int tag, const dim_vector& dv, const void *x)
    {
      int ndim = dv.length ();
      std::vector<size_t> dim (ndim);

      for (int k = 0; k < ndim; ++k)
	dim[k] = static_cast<size_t> (dv (k));

      size_t unframed_len = mtxenc_worst_uncompressed_len (tag, ndim, &dim[0]);
      std::vector<uint8_t> unframed (unframed_len);
      if (!mtxenc_pack (&unframed[0], &unframed_len, tag, ndim, &dim[0], x))
	return false;
      size_t framed_len = mtxenc_worst_framed_len (&unframed[0], unframed_len);
      encblob.resize (framed_len);
      if (!mtxenc_frame (&encblob[0], &framed_len, &unframed[0], unframed_len))
	return false;
      return true;
    }

    void encode (const uint8NDArray& x)
    {
      if (!do_encode (MTXCDC_UINT8, x.dims (), x.fortran_vec ()))
	throw std::string ("Failed to encode array.");
    }

    void encode (const NDArray& x)
    {
      if (!do_encode (MTXCDC_REAL_DOUBLE, x.dims (), x.fortran_vec ()))
	throw std::string ("Failed to encode array.");
    }
    void encode (const ComplexNDArray& x)
    {
      if (!do_encode (MTXCDC_COMPLEX_DOUBLE, x.dims (), x.fortran_vec ()))
	throw std::string ("Failed to encode array.");
    }

    void encode (const int32NDArray& x)
    {
      if (!do_encode (MTXCDC_INT32, x.dims (), x.fortran_vec ()))
	throw std::string ("Failed to encode array.");
    }
  };

  template <typename ot>
  octave_value do_blob_decode (const dim_vector& dv, const uint8_t* unframed)
  {
    ot out (dv);
    mtxdec_decode_data (out.fortran_vec (), unframed);
    return octave_value (out);
  }

  octave_value blob_decode (const uint8_t* framed, const size_t)
  {
    size_t unframed_len = mtxdec_unframed_len (framed);
    std::vector<uint8_t> unframed (unframed_len);
    if (!mtxdec_unframe (&unframed[0], framed))
      throw std::string ("Failed to unframe array.");

    int tag, ndim;
    size_t *dim = NULL;

    mtxdec_decode_header (&tag, &ndim, &dim, &unframed[0]);
    dim_vector dv (ndim);
    for (int k = 0; k < ndim; ++k)
      dv (k) = dim[k];
    std::free (dim);

    switch (tag) {
    case MTXCDC_UINT8:
      return do_blob_decode<uint8NDArray> (dv, &unframed[0]);
    case MTXCDC_INT32:
      return do_blob_decode<int32NDArray> (dv, &unframed[0]);
    case MTXCDC_REAL_DOUBLE:
      return do_blob_decode<NDArray> (dv, &unframed[0]);
    case MTXCDC_COMPLEX_DOUBLE:
      return do_blob_decode<ComplexNDArray> (dv, &unframed[0]);
    }
    throw std::string ("Failed to decode array.");
    return octave_value ();
  }

  typedef enum { INT_PARAM, INT64_PARAM, DOUBLE_PARAM, TEXT_PARAM,
		 BLOB_PARAM, NULL_PARAM, UNKNOWN_PARAM = -1 }
    /* Supported SQLite types */
    param_type_t;

  param_type_t
  map_type (const octave_value& val, const bool force_str = false)
  /* Assign an SQLite type to an Octave value.  If force_str is true,
     unrecognized types are converted to strings with a warning.
     Complex numbers could lose precision, etc., and so the default is
     to return UNKNOWN_PARAM.  The matrix types below are accepted and
     encoded / decoded.  The integer types other than uint8 are mapped
     to and from int32, except that int64 and uint32 arrays currently
     are not supported.

     | Octave type             | SQLite type                   |
     |-------------------------+-------------------------------|
     | scalar                  | double                        |
     | bool                    | int                           |
     | int64, uint32           | int64                         |
     | uint64                  | unknown or text               |
     | int smaller than uint32 | int                           |
     | string                  | text                          |
     | uint8_matrix            | blob(encoded)                 |
     | int32_matrix            | blob(encoded)                 |
     | int{8,16}_matrix        | blob(encoded), out as int32   |
     | uint16_matrix           | blob(encoded), out as int32   |
     | range                   | blob(encoded), out as NDArray |
     | NDArray                 | blob(encoded)                 |
     | ComplexNDArray          | blob(encoded)                 |
     | other                   | unknown or text               |
  */
  {
    int errc = 0;
    const int tid = val.type_id ();
    if (tid == octave_scalar::static_type_id ()) return DOUBLE_PARAM;
    if (tid == octave_bool::static_type_id ()) return INT_PARAM;
    if (val.is_integer_type ())
      {
	if (tid == octave_int64_scalar::static_type_id ()
	    || tid == octave_uint32_scalar::static_type_id ())
	  return INT64_PARAM;
	if (tid != octave_uint64_scalar::static_type_id ())
	  return INT_PARAM;
      }
    if (val.is_string ()) return TEXT_PARAM;

    if (blob_handles (tid)) return BLOB_PARAM;
    if (tid == octave_cell::static_type_id ()) {
      const octave_value& inner_val = val.cell_value ().operator() (0);
      if (blob_handles (inner_val.type_id ())) return BLOB_PARAM;
    }

    if (!force_str) return UNKNOWN_PARAM;

    warning ("Unsupported bind type %d, converting to string.", tid);
    return TEXT_PARAM;
  }

  struct sqlite3_value
  /* A kind of tagged union.  An sqlite3_value can only accept types
     convertable (see map_type), so we do not need a fully general
     solution like Boost's any. */
  {
    param_type_t tag;

    union {
      int int_value;
      int64_t int64_value;
      double double_value;
    };
    std::string text_value;
    blob_ov blob_value;

    sqlite3_value () : tag (NULL_PARAM) {}

    sqlite3_value (const octave_value& val)
    { set_value (val); }

    sqlite3_value (const octave_value& val, bool force_str)
    { set_value (val, force_str); }

    sqlite3_value& operator= (const sqlite3_value& val)
    {
      tag = val.tag;
      switch (tag)
	{
	case INT_PARAM: int_value = val.int_value; break;
	case INT64_PARAM: int64_value = val.int64_value; break;
	case DOUBLE_PARAM: double_value = val.double_value; break;
	case TEXT_PARAM: text_value = val.text_value; break;
	case BLOB_PARAM: blob_value = val.blob_value; break;
	}
      return *this;
    }

    sqlite3_value& operator= (const octave_value& val)
    { set_value (val); return *this; }

    void set_value (const octave_value& val, bool force_str = false)
    {
      tag = map_type (val, force_str);
      if (tag == UNKNOWN_PARAM)
	throw std::string ("Unconvertable type: " + val.type_name ());
      if (tag == NULL_PARAM)
	return;
      switch (tag)
	{
	case INT_PARAM: int_value = val.int_value (); break;
	case INT64_PARAM: int64_value = val.int64_scalar_value (); break;
	case DOUBLE_PARAM: double_value = val.double_value (); break;
	case TEXT_PARAM: text_value = val.string_value (); break;
	case BLOB_PARAM: blob_value.encode (val); break;
	}
    }

    void bind (sqlite3_stmt* stmt, const int k)
    /* Bind the current value to the kth slot in statement stmt.
       Dispatches for the current type stored in *this.  Strings and
       blobs must not be moved or deallocated for the lifetime of the
       binding.  That is sufficient for all current uses in this
       file, and the interface is not exported. */
    {
      int errc = SQLITE_MISUSE;
      switch (tag)
	{
	case INT_PARAM:
	  errc = sqlite3_bind_int (stmt, k, int_value); break;
	case INT64_PARAM:
	  errc = sqlite3_bind_int64 (stmt, k, int64_value); break;
	case DOUBLE_PARAM:
	  errc = sqlite3_bind_double (stmt, k, double_value); break;
	case TEXT_PARAM:
	  errc = sqlite3_bind_text (stmt, k, text_value.c_str (),
				    text_value.size (), SQLITE_STATIC);
	  break;
	case BLOB_PARAM:
	  errc = sqlite3_bind_blob (stmt, k, blob_value.fortran_vec (),
				    blob_value.numel (), SQLITE_STATIC);
	  break;
	case NULL_PARAM:
	  errc = sqlite3_bind_null (stmt, k); break;
	}
      if (errc) throw errc;
    }
  };
  // }}} SQLite3 values <-> Octave values

  // {{{ Statement helpers
  sqlite3_stmt*
  make_stmt (sqlite3* db, const std::string& q)
  /* Helper to handle errors from preparing q. */
  {
    sqlite3_stmt* stmt;
    if (osdbi::is_tracing ())
      std::cerr << "Q : " << q << std::endl;
    int errc = sqlite3_prepare_v2 (db, q.c_str (), q.size (), &stmt, NULL);
    if (errc) throw errc;
    return stmt;
  }

  struct wrapstmt
  /* General statement resource handler and binder.  Used for
     implementation inheritance. */
  {
    sqlite3_stmt* stmt;
    std::vector<sqlite3_value> v; /* Value cache for binding. */

    wrapstmt () : stmt (NULL) { }
    wrapstmt (sqlite3* db, const std::string& q) { set_query (db, q); }
    wrapstmt (sqlite3_stmt* stmt) : stmt (stmt) { v.resize (nparam ()); }
    ~wrapstmt () { if (stmt) sqlite3_finalize (stmt); }

    void
    set_query (sqlite3* db, const std::string& q)
    { stmt = make_stmt (db, q); v.resize (nparam ()); }
    void
    set_query (sqlite3_stmt* s)
    { stmt = s; v.resize (nparam ()); }

    void reset () { int errc = sqlite3_reset (stmt); if (errc) throw errc; }

    /* Output queries can have bindable parameters, but insertions
       don't return results. */
    int ncol () { return sqlite3_column_count (stmt); }
    std::string colname (int k) { return sqlite3_column_name (stmt, k); }
    int nparam () { return sqlite3_bind_parameter_count (stmt); }
    std::string paramname (int k)
    { return sqlite3_bind_parameter_name (stmt, k); }

    /* Dispatch through the value cache. */
    void bind (const int f, const octave_value& val)
    { v[f] = val; v[f].bind (stmt, f+1); }
    void bind (const int f, octave_value& val, const octave_idx_type k)
    {
      octave_value tmp;
      if (val.is_cell ())
	{
	  Cell ctmp = val.cell_value ();
	  tmp = ctmp.elem (k);
	}
      else
	tmp = val.do_index_op (octave_value (k+1));
      v[f] = tmp;
      v[f].bind (stmt, f+1);
    }

  };
  // }}} Statement helpers

  // {{{ Insertion statement
  struct insstmt : protected wrapstmt
  /* Insertion statement.  The output column count routines are not
     exposed.  The exec method fires a single insertion query and
     throws on error. */
  {
    insstmt (sqlite3* db, const std::string& tname,
	     const std::string& field_list, const size_t nf)
    {
      std::string q
	= "INSERT INTO " + tname + " ( " + field_list + " ) VALUES ( ?";
      for (int k = 1; k < nf; ++k)
	q += ", ?";
      q += " );";
      set_query (db, q);
      assert (nparam () == nf);
    }

    using wrapstmt::reset;
    using wrapstmt::nparam;
    using wrapstmt::paramname;
    using wrapstmt::bind;

    void exec ()
    {
      int errc = sqlite3_step (stmt);
      if (SQLITE_DONE == errc) { reset (); return; }
      if (SQLITE_INTERRUPT == errc) throw -1;
      if (SQLITE_ROW == errc)
	throw std::string("Returned data with no destination.");
      throw errc;
    }
  };
  // }}} Insertion statement

  // {{{ Gather/query statement
  void
  do_append(osdbi::automutating_column& col, sqlite3_stmt* stmt, int k)
  /* Append the kth result column from stmt into an automutating
     column.  Converts from SQLite types to osdbi-supported Octave
     types. */
  {
    switch (sqlite3_column_type (stmt, k))
      {
      case SQLITE_INTEGER:
	{
	  int64_t v = sqlite3_column_int64 (stmt, k);
	  if (v < std::numeric_limits<int>::max ()
	      && v > std::numeric_limits<int>::min ())
	    col.append (static_cast<int> (v));
	  else
	    col.append (static_cast<octave_int64> (v));
	}
	break;
      case SQLITE_FLOAT:
	col.append (sqlite3_column_double (stmt, k));
	break;
      case SQLITE_TEXT:
	{
	  const char* text
	    = reinterpret_cast<const char*> (sqlite3_column_text (stmt, k));
	  const size_t nb = sqlite3_column_bytes (stmt, k);
	  col.append (std::string (text, nb));
	}
	break;
      case SQLITE_BLOB:
	{
	  const uint8_t* blob
	    = reinterpret_cast<const uint8_t*> (sqlite3_column_blob (stmt, k));
	  const size_t nb = sqlite3_column_bytes (stmt, k);
	  col.append (blob_decode (blob, nb));
	}
	break;
      case SQLITE_NULL:
	col.append ();
	break;
      default:
	throw std::string ("Unknown SQLite type.");
      }
  }

  struct gatherstmt : protected wrapstmt
  /* The query statement.  The exec method runs through the entire
     dataset and throws on errors.  Each column of the query is
     gathered and made available through the column method. If a
     result is a scalar, column() also should return a scalar through
     Octave's maybe_mutate magic.  The to_map method returns every
     column in an Octave structure with fields named according to the
     active statement's output.  That can result in illegal field
     names if they contain periods. */
  {
    std::vector<osdbi::automutating_column> vout;

    gatherstmt (sqlite3* db, const std::string& q)
      : wrapstmt (db, q), vout (ncol ()) {}

    gatherstmt (sqlite3_stmt* stmt)
      : wrapstmt (stmt), vout (ncol ()) { }

    using wrapstmt::ncol;
    using wrapstmt::colname;
    using wrapstmt::reset;
    using wrapstmt::nparam;
    using wrapstmt::paramname;
    using wrapstmt::bind;

    void
    exec ()
    {
      const int nf = ncol ();
      for (int rw = 1; ; ++rw)
	{
	  OCTAVE_QUIT;
	  int errc = sqlite3_step (stmt);
	  if (SQLITE_DONE == errc) { reset (); return; }
	  if (SQLITE_INTERRUPT == errc) throw -1;
	  if (SQLITE_BUSY == errc) continue;
	  if (SQLITE_ROW != errc) throw errc;
	  for (int k = 0; k < nf; ++k) do_append (vout[k], stmt, k);
	}
    }

    octave_value
    column (int k)
    {
      if (k >= vout.size ()) return octave_value ();
      octave_value out (vout[k].get_octave_value ());
      out.maybe_mutate ();
      return out;
    }

    void
    to_map (Octave_map& out)
    /* XXX: Should this filter "."s out of names? */
    {
      const int nf = ncol ();
      for (int k = 0; k < nf; ++k) {
	std::string fname (colname (k));
	octave_value fval (column (k));
	out.assign (fname, fval);
      }
    }
  };
  // }}} Gather/query statement

  // {{{ Temporary table
  struct temptable
  /* Temporary table used for inserting a batch of records and then
     gathering the results. */
  {
    sqlite3 *db;
    std::string tname;
    std::vector<std::string> fname;
    std::string fname_list;
    Cell fval;

    temptable (sqlite3* db,
	       const char *intname,
	       const Octave_map& val)
      : db (db), tname (intname)
    {
      if (val.nfields ())
	{
	  gather_fields (val);
	  size_t nfields = fname.size ();
	  std::string q =
	    "CREATE TEMPORARY TABLE " + tname + " ( " + fname_list + " );";
	  do_arb_exec (db, q);
	  do_insert ();
	}
      else
	db = NULL; // Empty, don't create a table at all.
    }

    ~temptable ()
    {
      if (db)
	{
	  std::string q = "DROP TABLE ";
	  q += tname + ";";
	  do_arb_exec (db, q, false);
	}
    }

    void
    gather_fields (const Octave_map& fld)
    {
      octave_idx_type nf = fld.nfields ();
      fname.clear (); fname.resize (nf);
      fval.resize (nf);
      int k = 0;
      std::string fnm;
      for (Octave_map::const_iterator it = fld.begin ();
	   it != fld.end (); ++it, ++k)
	{
	  fnm = fld.key (it);
	  fval(k) = fld.contents (fnm)(0);
	  fname[k] = "[" + fnm + "]";
	  if (k) fname_list += ", ";
	  fname_list += fname[k];
	}
    }

    void
    do_insert ()
    {
      const octave_idx_type nfields = fname.size ();

      std::vector<octave_idx_type> flen (nfields);
      octave_idx_type ndata = 0;
      for (int f = 0; f < nfields; ++f)
	if (!fval(f).is_string ())
	  {
	    flen[f] = fval(f).numel ();
	    switch (flen[f])
	      {
	      case 1:
		if (ndata < 1) ndata = 1;
	      case 0:
		break;
	      default:
		if (ndata > 1 && ndata != flen[f])
		  throw std::string("Unmatched field lengths.");
		ndata = flen[f];
		break;
	      }
	  }
      for (int f = 0; f < nfields; ++f)
	if (fval(f).is_string ())
	  {
	    octave_idx_type ln = fval(f).numel ();
	    if (ndata == ln) // treat as an array of characters
	      flen[f] = ndata;
	    else // treat as a single string
	      flen[f] = 1;
	  }
      if (!ndata) return;

      insstmt ins (db, tname, fname_list, nfields);

      do_arb_exec (db, "BEGIN;");
      try {
	for (int k = 0; k < ndata; ++k)
	  {
	    for (int f = 0; f < nfields; ++f) {
	      octave_value tmp = fval.elem (f);
	      switch (flen[f])
		{
		case 1:
		  ins.bind (f, tmp);
		case 0:
		  break;
		default:
		  ins.bind (f, tmp, k);
		  break;
		}
	    }
	    ins.exec ();
	  }
      }
      catch (...) {
	if (!sqlite3_get_autocommit (db)) do_arb_exec (db, "ROLLBACK;");
	throw;
      }
      if (!sqlite3_get_autocommit (db)) do_arb_exec (db, "COMMIT;");
    }
  };
  // }}} Temporary table

} // anonymous namespace

// {{{ Large sqlite3_conn routines
int
sqlite3_conn::insert (const std::string& tname,
		      const Octave_map& indata,
		      Octave_map* outdata)
/* The strategy is to build a temporary table based on indata and
   insert that temporary table into tname in one statement.  The
   insertion strips out duplicates and is idempotent.  An inner join back
   with the temporary table gathers the output data.  The insertion is
   atomic, but the gap between the insertion and the join may produce
   NULL values if some process deletes the relevant rows. */
{
  if (!is_open ())
    return do_errc (SQLITE_MISUSE, "Connection not open.");

  /* Temporary table name.  This library already assumes single-
     threading in many places, so using a constant is relatively
     safe.  Them temptable class handles dropping the table on scope
     exit. */
  static const char* iname = "osdbiins";
  try
    {
      temptable instbl (db, iname, indata);
      std::string q;

      q = "INSERT INTO " + tname + " ( " + instbl.fname_list + " ) "
	"SELECT " + instbl.fname_list + " FROM ";
      q += iname;
      q += " EXCEPT SELECT " + instbl.fname_list + " FROM " + tname + ";";
      do_arb_exec (db, q);

      if (outdata && outdata->nfields ()) {
	q = "SELECT ";
	Octave_map::const_iterator it = outdata->begin ();
	q += "[" + outdata->key (it++) + "]";
	for (Octave_map::const_iterator it_end = outdata->end ();
	     it != it_end; ++it)
	  q += ", [" + outdata->key (it) + "]";
	q += " FROM ";
	q += iname;
	q += " NATURAL INNER JOIN " + tname + ";";
	gatherstmt rowgather (db, q);
	rowgather.exec ();
	rowgather.to_map (*outdata);
      }
    }
  catch (int errc)
    {
      if (errc > 0)
	return do_errc (errc, sqlite3_errmsg (db));
      else
	return do_errc (errc); // Interrupted or ok.
    }
  catch (const std::string& msg)
    {
      return do_errc (SQLITE_MISUSE, msg);
    }

  return 0;
}

int
sqlite3_conn::expand (const std::string& tname,
		      const Octave_map& indata,
		      Octave_map& outdata,
		      const Octave_map* noteq)
/* Similar to insertion, expansion creates a temporary table and uses
   an inner join for producing the results.  The benefit is that values
   are converted into SQLite form *once*, but the drawback is that
   the input values are duplicated in memory.  */
{
  static const char* iname = "osdbiexp";
  static const char* noteqname = "osdbinoteq";

  if (!is_open ())
    return do_errc (SQLITE_MISUSE, "Connection not open.");

  const int nf = outdata.nfields ();
  const int innf = indata.nfields ();
  const int neqf = (noteq? noteq->nfields() : 0);

  std::string q = "SELECT ";
  if (nf)
    {
      int k = 0;
      for (Octave_map::const_iterator it = outdata.begin (); k < nf; ++it, ++k)
	{
	  if (k) q += ", ";
	  q += outdata.key (it);
	}
    }
  else
    q += "*";
  q += " FROM ";
  if (innf)
    {
      q += iname;
      q += " NATURAL INNER JOIN ";
    }
  if (neqf)
    {
      q += "(SELECT " + tname + ".* FROM " + tname
	+ " LEFT OUTER NATURAL JOIN ";
      q += noteqname;
      q += " WHERE ";
      int k = 0;
      for (Octave_map::const_iterator it = noteq->begin (); k < neqf;
	   ++it, ++k)
	{
	  if (k) q += " AND ";
	  q += noteqname;
	  q += "." + noteq->key (it);
	  q += " IS NULL ";
	}
      q += ");";
    }
  else
    q += tname + ";";

  try
    {
      if (!neqf)
	{
	  temptable instbl (db, iname, indata);
	  gatherstmt gstmt (db, q);
	  gstmt.exec ();
	  gstmt.to_map (outdata);
	}
      else
	{
	  temptable instbl (db, iname, indata);
	  temptable neqtbl (db, noteqname, *noteq);
	  gatherstmt gstmt (db, q);
	  gstmt.exec ();
	  gstmt.to_map (outdata);
	}
    }
  catch (int errc)
    {
      if (errc > 0)
	return do_errc (errc, sqlite3_errmsg (db));
      else
	return do_errc (errc); // Interrupted or ok.
    }
  catch (const std::string& msg)
    {
      return do_errc (SQLITE_MISUSE, msg);
    }

  return 0;
}

int
sqlite3_conn::arbitrary_exec (const std::string& qstring,
			      const Cell& params,
			      octave_value_list& outvals)
/* arbitrary_exec can be used for complicated queries with simple
   arguments.  There is no temporary table or join; all the arguments
   are scalars that are bound once. */
{
  try
    {
      gatherstmt stmt (db, qstring);
      const octave_idx_type np = params.nelem ();
      if (np != stmt.nparam ())
	throw std::string("Incorrect number of parameters.");
      for (int k = 0; k < np; ++k)
	stmt.bind (k, params(k));
      stmt.exec ();
      const int nc = stmt.ncol ();
      string_vector name (nc);
      outvals.resize (nc);
      for (int k = 0; k < nc; ++k)
	{
	  name (k) = stmt.colname (k);
	  outvals (k) = stmt.column(k);
	}
      outvals.stash_name_tags (name);
      return 0;
    }
  catch (int errc)
    {
      if (errc > 0)
	return do_errc (errc, sqlite3_errmsg (db));
      else
	return do_errc (errc); // Interrupted or ok.
    }
  catch (const std::string& msg)
    {
      return do_errc (SQLITE_MISUSE, msg);
    }
}
// }}}

// PKG_ADD: autoload ("osdbi_sqlite3_make", "osdbi.oct");
// PKG_ADD: mlock ();
DEFUN_DLD (osdbi_sqlite3_make, args, ,
	   "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {} osdbi_sqlite3_make ()\n\
Internal connection creator.  Call via @var{osdbi}.\n\
@example\n\
@code{osdbi(\"sqlite3\");}\n\
@end example\n\
@seealso{osdbi}\n\
@end deftypefn")
{
  const int nargin = args.length ();
  sqlite3_conn* sqlite3conn = new sqlite3_conn;
  octave_osdbi_conn* out = new octave_osdbi_conn (sqlite3conn);

  return octave_value (out);
}
