// -*- mode: C++; c-file-style: "gnu" -*-
#ifndef MYSDBI_HEADER_
#define MYSDBI_HEADER_ 1

#include <string>
#include <memory>

#include <octave/config.h>
#include <octave/oct-map.h>
#include <octave/int64NDArray.h>

// Virtual interface class
namespace osdbi {

  /* These are magic globals.  There is no specification of exactly
     what tracing *does*; it's meant for vague debugging purposes. */
  bool is_tracing ();
  bool set_tracing (bool);
  void toggle_tracing ();

  class conn_interface
  /* Abstract database connection interface.  The overall goal is to
     provide an easy interface for inserting data into a database and
     retrieving subsets for analysis in Octave.  This interface is
     not intended to support heavy database-side analytical work.
     Thus there is one insertion interface and one query / "expansion"
     interface.  There is a pass-through routine in arbitrary_exec
     intended for simple work like creating tables and indices, but
     the semantics are back-end specific. */
  {
  public:
    conn_interface ();
    virtual ~conn_interface () = 0;
  protected:
    conn_interface (const conn_interface&);
    conn_interface& operator= (const conn_interface&);
    void swap (conn_interface&);
  public:

    virtual conn_interface* clone () = 0;

    /* Options are driver-specific.  They are intended for passing
       database or file names, user names, read-write flags, etc. */
    virtual int set_option (const std::string& /* key */,
			    const std::string& /* val */) = 0;
    virtual int set_option (const std::string& /* key */,
			    const int /* val */) = 0;
    /* Retrieve a set of options for later reference.  There is
       no set-from-map facility at the connection level; maps are
       converted to appropriate set_option calls in the Octave
       interface. */
    virtual Octave_map get_options () const = 0;

    virtual int open () = 0;
    virtual int close () = 0;

    virtual bool is_open () const = 0;
    virtual bool is_readonly () const = 0;
    virtual bool is_ok () const = 0;

    virtual std::string driver_name () const = 0;
    virtual std::string current_description () const = 0;
    virtual std::string current_status () const = 0;

  public:
    /* Insert data and optionally retrieve information about the
       data, e.g. generated row ids. */
    virtual int insert (const std::string& /* table name */,
			const Octave_map& /* data to insert */,
			Octave_map* /* output */ = NULL) = 0;

    /* Take an input map and return all records in the table matching
       the input values exactly. */
    virtual int expand (const std::string& /* table name */,
			const Octave_map& /* input field values */,
			Octave_map& /* output */,
			const Octave_map* /* not= constraints */ = 0) = 0;

    /* Take an arbitrary query, bind the parameters in order from the
       passed cell array, and returns values in a value list. */
    virtual int arbitrary_exec (const std::string& /* qstring */,
				const Cell& /* params */,
				octave_value_list& /* output */) = 0;
    /* Just throw a string at the database. */
    virtual int arbitrary_exec (const std::string& /* qstring */) = 0;
  };
} // namespace osdbi

// Octave type for osdbi connections
class octave_osdbi_conn : public octave_base_value
/* We use an entire type to piggyback on Octave's value semantics and
   possibly to provide for dispatching.  Octave controls the
   connection extent cleanly.  There are painful problems if someone
   replaces the oct-file while running, but those seem unavoidable
   without severe hackery. */
{
public:
  typedef osdbi::conn_interface conn_interface;

  octave_osdbi_conn () : conn (NULL) { }
  octave_osdbi_conn (conn_interface* conn) : conn (conn) { }

  octave_osdbi_conn (const octave_osdbi_conn& other)
    : conn (other.conn->clone ())
  {}

  octave_osdbi_conn& operator= (const octave_osdbi_conn& other);

  ~octave_osdbi_conn () { finish (); }

  octave_base_value *clone () const { return new octave_osdbi_conn (*this); }
  octave_base_value *empty_clone () const { return new octave_osdbi_conn (); }

  bool is_defined () const { return NULL != conn; }

  virtual bool is_scalar_type () const { return true; }
  // XXX: Should is_true be linked to is_defined or is_open?
  virtual bool is_true () const { return is_defined (); }

  /* Dump the pointer out for debugging. */
  virtual octave_uint64 uint64_scalar_value () const
  { return octave_uint64 ((intptr_t)conn) ; }

  virtual void print (std::ostream&, bool) const;
  virtual void print_raw (std::ostream&, bool) const;

  conn_interface* get_conn () const { return conn; }
  void set_conn (conn_interface* newconn) { finish (); conn = newconn; }

protected:
  conn_interface* conn;
  void finish ();

  DECLARE_OCTAVE_ALLOCATOR

  DECLARE_OV_TYPEID_FUNCTIONS_AND_DATA
};

#endif /* MYSDBI_HEADER_ */
